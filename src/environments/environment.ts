// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false
};

export const firebaseConfig = {
  apiKey: "AIzaSyAOVnlgu7g_lKInk3QSWJQHq2r5XPWFZW8",
  authDomain: "database-1a5ff.firebaseapp.com",
  databaseURL: "https://database-1a5ff.firebaseio.com",
  projectId: "database-1a5ff",
  storageBucket: "database-1a5ff.appspot.com",
  messagingSenderId: "423850152083",
  appId: "1:423850152083:web:23ba7e17bf6e176018fd66",
  measurementId: "G-SYH28GWL2M"
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
