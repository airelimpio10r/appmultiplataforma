import { Component, OnInit} from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ModalController} from "@ionic/angular";
import { AuthService } from 'src/app/core/service/auth.service';
import { FirestoreCRUDService } from 'src/app/core/service/firestore/firestore-crud.service';
import { Sponsor } from 'src/app/shared/patrocinio.interface';
import { LogicReuseService } from 'src/app/shared/resources/logic-reuse.service';
@Component({
  selector: 'app-modal-image',
  templateUrl: './modal-image.page.html',
  styleUrls: ['./modal-image.page.scss'],
})
export class ModalImagePage implements OnInit {

  constructor(
    private modalCtrl : ModalController
  ) {}

  cerrarModal(){
    this.modalCtrl.dismiss();
  }

  ngOnInit() {
  }


}
