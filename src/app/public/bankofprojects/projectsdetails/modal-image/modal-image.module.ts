import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ModalImagePageRoutingModule } from './modal-image-routing.module';

import { ModalImagePage } from './modal-image.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ModalImagePageRoutingModule,
    ReactiveFormsModule
  ],
  declarations: [ModalImagePage]
})
export class ModalImagePageModule {}
