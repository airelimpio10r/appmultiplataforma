import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { ModalImagePage } from './modal-image/modal-image.page';
import { ModalGaleryPage } from './modal-galery/modal-galery.page';
import { Router } from '@angular/router';
import { FirestoreCRUDService } from 'src/app/core/service/firestore/firestore-crud.service';
import { AuthService } from 'src/app/core/service/auth.service';


@Component({
  selector: 'app-projectsdetails',
  templateUrl: './projectsdetails.page.html',
  styleUrls: ['./projectsdetails.page.scss'],
})
export class ProjectsdetailsPage implements OnInit {

  detailsDoc = this.CRUD.detailsProduct

  constructor(
    private modalCtrl: ModalController, private CRUD: FirestoreCRUDService, private router: Router,

  ) { }

  ngOnInit() {
    if (this.detailsDoc.length == 0) {
      this.router.navigate(['/allprojects'])
    }
  }

  async popUp() {
    const modal = this.modalCtrl.create({
      component: ModalImagePage,
      cssClass: 'my-modal2-css',
    });
    return (await modal).present()
  }

  async popUpGalery() {
    const modal = this.modalCtrl.create({
      component: ModalGaleryPage,
      cssClass: 'my-modal-galery-css',
    });
    return (await modal).present()
  }

}
