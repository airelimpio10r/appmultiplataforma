import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { IonSlides, ModalController, NavParams } from '@ionic/angular';
import { FirestoreCRUDService } from 'src/app/core/service/firestore/firestore-crud.service';

@Component({
  selector: 'app-modal-galery',
  templateUrl: './modal-galery.page.html',
  styleUrls: ['./modal-galery.page.scss'],
})
export class ModalGaleryPage implements OnInit {
  imagenes = []
  imgURL: any = [];

  @ViewChild("slider", { read: ElementRef, static: false }) slider: ElementRef;
  @ViewChild(IonSlides, { static: true }) slides: IonSlides;

  sliderOpts = {
    initialSlide: 0,
    slidesPerView: 1,
    loop: true,
    spaceBetween: 15,
    loopedSlides: 5,
    zoom: {
      maxRatio: 3
    }
  };

  constructor(private navParams: NavParams,
    private modalCtrl: ModalController, private CRUD: FirestoreCRUDService,
  ) {
    this.imagenes = [];
    this.CRUD.detailsProduct.forEach(data => {
      data.photos.forEach(element => {
        this.imagenes.push(element)
      });
    });
  }

  cerrarModal() {
    this.modalCtrl.dismiss();
  }
  slideTo(index) {
    this.slider.nativeElement.slideTo(index);
  }
  zoom(ZoomIn: boolean) {
    let zoom = this.slider.nativeElement.swiper.zoom;
    if (ZoomIn) {
      zoom.in()
    } else {
      zoom.out();
    }
  }

  ngOnInit() {
    this.slides.update();
  }

}
