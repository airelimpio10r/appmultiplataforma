import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { AllprojectsPageRoutingModule } from './allprojects-routing.module';

import { AllprojectsPage } from './allprojects.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    AllprojectsPageRoutingModule,
  ],
  declarations: [AllprojectsPage]
})
export class AllprojectsPageModule {}
