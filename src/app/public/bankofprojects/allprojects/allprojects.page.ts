import { Component, OnInit } from '@angular/core';
import { RegisterProject } from 'src/app/shared/registrarproyecto.interface';
import { Router } from '@angular/router';
import { FirestoreCRUDService } from 'src/app/core/service/firestore/firestore-crud.service';

@Component({
  selector: 'app-allprojects',
  templateUrl: './allprojects.page.html',
  styleUrls: ['./allprojects.page.scss'],
})
export class AllprojectsPage implements OnInit {

  collectionProject: RegisterProject[] = []

  constructor(
    private CRUD: FirestoreCRUDService, private router: Router,
  ) {
  }

  ngOnInit() {
    this.loadData()
  }

  //Activar el refres
  doRefresh(event) {
    this.loadData()
    setTimeout(() => {
      event.target.complete();
    }, 2000);
  }
  //Enviar datos a otra page
  public detailsProject(data) {
    this.CRUD.detailsProduct = []
    this.CRUD.detailsProduct.push(data);
    this.router.navigate([`/allprojects/projectsdetails/${data.projectName}`]);
  }
  // Cargar proyectos
  loadData() {
    this.CRUD.getAllDocument().subscribe(value => {
      this.collectionProject = [];
      value.docs.forEach(element => {
        const data = element.data()
        if(data.Validate == "Aprobado"){
          const document: RegisterProject = {
            projectType: data.projectType,
            projectName: data.projectName,
            forestArea: data.forestArea,
            forestType: data.forestType,
            temperature: data.temperature,
            typeTemperature: data.typeTemperature,
            treeDiameter: data.treeDiameter,
            treeAge: data.treeAge,
            treeSize: data.treeSize,
            country: data.country,
            department: data.department,
            municipality: data.municipality,
            zone: data.zone,
            msnm: data.msnm,
            propertyName: data.propertyName,
            cadastralCertificate: data.cadastralCertificate,
            enrollment: data.enrollment,
            treeSpecies: data.treeSpecies,
            photos: data.photos,
            date: data.date,
            Validate: data.Validate
          }
          this.collectionProject.push(document);
        }
      })
    })
  }

}
