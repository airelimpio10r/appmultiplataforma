import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { AllprojectsPage } from './allprojects.page';

describe('AllprojectsPage', () => {
  let component: AllprojectsPage;
  let fixture: ComponentFixture<AllprojectsPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AllprojectsPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(AllprojectsPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
