import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { LogicReuseService } from 'src/app/shared/resources/logic-reuse.service';

@Component({
  selector: 'app-intro',
  templateUrl: './intro.page.html',
  styleUrls: ['./intro.page.scss'],
})
export class IntroPage implements OnInit {


  constructor(
    private router: Router, private logicReuse: LogicReuseService
  ) { }

  ngOnInit() {
  }

  scrollToBtt(value) {
    if (value == 0) {
      this.router.navigate(['/home'])
    } else {
      this.logicReuse.scrollNumber = value;
      this.router.navigate(['/plan-accion'])
    }
  }

}
