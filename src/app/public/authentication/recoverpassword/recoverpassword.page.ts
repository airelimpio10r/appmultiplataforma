import { Component, OnInit } from '@angular/core';

import { ToastController, ModalController } from '@ionic/angular';
import { AuthService } from '../../../core/service/auth.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { LogicReuseService } from 'src/app/shared/resources/logic-reuse.service';


@Component({
  selector: 'app-recoverpassword',
  templateUrl: './recoverpassword.page.html',
  styleUrls: ['./recoverpassword.page.scss'],
})
export class RecoverpasswordPage implements OnInit {

  forgotPassword: FormGroup;
  validation_messages

  constructor(
    private authServise: AuthService, private logicReuse: LogicReuseService,
    private formBuilder: FormBuilder, private router: Router,
  ) { }

  ngOnInit() {
    this.forgotPassword = this.FormforgotPassword();
    this.validation_messages = this.logicReuse.validation_messages
  }

  //Envia link de restauración de contraseña
  sendLinkReset(value) {
    this.authServise.resetPassword(value.email).then(() => {
      this.logicReuse.presentToastSuccess(' <ion-icon name="checkmark-circle"></ion-icon> Se envió un correo electrónico con un enlace para restablecer su contraseña.');
      this.router.navigate(['/login']);
    }).catch(err => alert(err));
  }
  //Condiciones de la entrada de datos form
  private FormforgotPassword() {
    return this.formBuilder.group({
      email: ['', [Validators.required, Validators.email]]
    })
  }
}
