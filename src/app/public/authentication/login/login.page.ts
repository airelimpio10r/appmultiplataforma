import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AuthService } from '../../../core/service/auth.service';
import { Router } from '@angular/router';
import { AppComponent } from 'src/app/app.component';
import { LogicReuseService } from 'src/app/shared/resources/logic-reuse.service';
import { ModalController } from '@ionic/angular';
import { EmailVerificationPage } from '../email-verification/email-verification.page';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

  hide = true;
  loginFrom: FormGroup;
  validation_messages;
  inputUntouched = false;

  constructor(
    private authService: AuthService, private router: Router, private modalCtrl: ModalController,
    private formBuilder: FormBuilder, private app: AppComponent, private logicReuse: LogicReuseService,

  ) { }


  ngOnInit() {
    this.loginFrom = this.createMyForm();
    this.validation_messages = this.logicReuse.validation_messages
  }

  //Evalua el uso del botton enter para hacer el submit
  eventHandler(keyCode) {
    if (keyCode === 13) {
      if (this.loginFrom.valid) {
        // Trigger the button element with a click
        document.getElementById("fancy-button").click();
      }
    }
  }
  //Login con Firebase (Correo y contraseña)
  async loginEmailandPassword(value) {
    try {
      this.authService.onLogin(value).then(user => {
        if (!user.emailVerified) {
          this.popUp();
        }
        this.router.navigate(['/home']);
      });
    } catch (error) {
      console.log('Error', error);
    }
  }
  // Donde se guardan los datos del form
  saveData(data) {
    if (this.loginFrom.valid) {
      this.loginEmailandPassword(data);
      this.loginFrom.reset();
      this.inputUntouched = false;
    } else {
      this.inputUntouched = true;
    }
  }
  // Condiciones de la entrada de datos form
  private createMyForm() {
    return this.formBuilder.group({
      email: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required]]
    });
  }

  // Recordatorio que no ha verificado el email
  async popUp() {
    const modal = this.modalCtrl.create({
      component: EmailVerificationPage,
      cssClass: 'my-emailv-css',
    });
    return (await modal).present()
  }
}
