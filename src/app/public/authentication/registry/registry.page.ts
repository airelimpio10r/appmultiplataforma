import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { AlertController, ModalController, ToastController } from '@ionic/angular';
import { AuthService } from '../../../core/service/auth.service';
import { LogicReuseService } from 'src/app/shared/resources/logic-reuse.service';
import { Router } from '@angular/router';
import { User } from 'src/app/shared/user.interface';
import { EmailVerificationPage } from '../email-verification/email-verification.page';

@Component({
  selector: 'app-registry',
  templateUrl: './registry.page.html',
  styleUrls: ['./registry.page.scss'],
})
export class RegistryPage implements OnInit {

  selectTabs = 'naturalPerson';
  RegistryPNatural: FormGroup;
  RegistryBusiness: FormGroup;
  activate = false;
  hide: boolean;
  hide1: boolean;
  hide2: boolean;
  validation_messages;
  options = ['Cédula de Ciudadanía', 'Cédula de Extranjería', 'Carné diplomatico', 'Pasaporte', 'Permiso especial de permanencia',];
  Actividad = ['Agricultura y ganadería', 'Bienes de consumo', 'Comercio electrónico', 'Construcción', 'Alimentación', 'Deporte y ocio',
    'Energía', 'Finanzas, seguros y bienes inmuebles', 'Lógistica y transporte', 'Medios de comunicación y marketing', 'Metalurgia y electrónica', 'Productos químicos y materias primas',
    'Salud e industria farmacéutica', 'Educación', 'Tecnología y telecomunicaciones', 'Turismo y hotelería', 'Fabricación y manufacturación',
  ];
  date: Date = new Date();

  constructor(
    private toastController: ToastController, private authService: AuthService,
    private formBuilder: FormBuilder, private logicReuse: LogicReuseService, private router: Router, private modalCtrl: ModalController,
  ) { }

  ngOnInit() {
    this.RegistryPNatural = this.FormPNatural();
    this.RegistryBusiness = this.FormEmpresa();
    this.validation_messages = this.logicReuse.validation_messages;
  }
  //Evalua el uso del botton enter para hacer el submit
  eventHandlerP(keyCode) {
    if (keyCode === 13) {
      if (this.RegistryPNatural.valid) {
        // Trigger the button element with a click
        document.getElementById("send").click();
      }
    }
  }
  //Evalua el uso del botton enter para hacer el submit
  eventHandlerB(keyCode) {
    if (keyCode === 13) {
      if (this.RegistryBusiness.valid) {
        // Trigger the button element with a click
        document.getElementById("send").click();
      }
    }
  }
  //Registrar Usuario
  async RegistroEmailPassword(value) {
    try {
      this.authService.onRegister(value).then(user => {
        if (user) {
          this.router.navigate(['/login']);
          this.popUp();
        };
      })
    } catch (error) {
      console.log(error);
    }
  }
  // Donde se guardan los datos del form
  saveDataPNatural(user) {
    const data: User = {
      name: user.name,
      lastName: user.lastName,
      photoProfile: {
        photoURL: 'https://firebasestorage.googleapis.com/v0/b/database-1a5ff.appspot.com/o/profile_picture%2FdefaultImage.png?alt=media&token=c2ef1066-fafd-42c4-bd55-15fb8f11dc26',
      },
      email: user.email,
      cellPhone: user.cellPhone,
      password: user.passwordRetry.passwordConfirmation,
      rol: 'P.Natural',
      date: this.date,
    };
    this.RegistroEmailPassword(data);
    this.RegistryPNatural.reset(); // borramos los datos del form
  }
  saveDataEmpresa(user) {
    const data: User = {
      name: user.name,
      lastName: user.lastName,
      photoProfile: {
        photoURL: 'https://firebasestorage.googleapis.com/v0/b/database-1a5ff.appspot.com/o/profile_picture%2FdefaultImage.png?alt=media&token=c2ef1066-fafd-42c4-bd55-15fb8f11dc26',
      },
      email: user.email,
      rol: 'Empresa',
      date: this.date,
      nit: user.nit,
      companyName: user.companyName,
      companyActivity: user.companyActivity,
      documentType: user.documentType,
      document: user.document,
      cellPhone: user.cellPhone,
      direction: user.direction,
      password: user.passwordRetry.passwordConfirmation,
    };
    this.RegistroEmailPassword(data);
    this.RegistryBusiness.reset(); // borramos los datos del form
  }
  // Verifica la contraseña en el form
  matchOtherValidator(otherControlName: string) {
    let thisControl: FormControl;
    let otherControl: FormControl;
    return function matchOtherValidate(control: FormControl) {
      if (!control.parent) {
        return null;
      }
      // Initializing the validator.
      if (!thisControl) {
        thisControl = control;
        otherControl = control.parent.get(otherControlName) as FormControl;
        if (!otherControl) {
          throw new Error('matchOtherValidator(): other control is not found in parent group');
        }
        otherControl.valueChanges.subscribe(() => {
          thisControl.updateValueAndValidity();
        });
      }
      if (!otherControl) {
        return null;
      }
      if (otherControl.value !== thisControl.value) {
        return {
          matchOther: true
        };
      }
      return null;
    }
  }
  // Condiciones de la entrada de datos form
  private FormEmpresa() {
    return this.formBuilder.group({
      nit: ['', [Validators.required, Validators.minLength(2)]],
      companyName: ['', [Validators.required, Validators.minLength(2)]],
      companyActivity: ['', [Validators.required]],
      name: ['', [Validators.required, Validators.pattern('[a-zA-Z ]*'), Validators.minLength(2)]],
      lastName: ['', [Validators.required, Validators.pattern('[a-zA-Z ]*'), Validators.minLength(2)]],
      cellPhone: ['', [Validators.required, Validators.minLength(10), Validators.maxLength(10), Validators.pattern('[0-9]*')]],
      documentType: ['', Validators.required],
      document: ['', [Validators.required, Validators.minLength(7), Validators.pattern('[A-Za-z0-9]*')]],
      direction: ['', [Validators.required, Validators.minLength(2)]],
      email: ['', [Validators.required, Validators.email]],
      passwordRetry: this.formBuilder.group({
        password: ['', [Validators.required, Validators.minLength(6), Validators.pattern('(?=.*\\d)(?=.*[0-9])(?=.*[a-záéíóúüñ]).*[A-ZÁÉÍÓÚÜÑ].*')]],
        passwordConfirmation: ['', [Validators.required, Validators.minLength(6), this.matchOtherValidator('password')]]
      }),
      termsAndConditions: [false, [Validators.requiredTrue]],
    });
  }
  // Condiciones de la entrada de datos form
  private FormPNatural() {
    return this.formBuilder.group({
      name: ['', [Validators.required, Validators.pattern('[a-zA-Z ]*'), Validators.minLength(2)]],
      lastName: ['', [Validators.required, Validators.pattern('[a-zA-Z ]*'), Validators.minLength(2)]],
      email: ['', [Validators.required, Validators.email]],
      cellPhone: ['', [Validators.required, Validators.minLength(10), Validators.maxLength(10), Validators.pattern('[0-9]*')]],
      passwordRetry: this.formBuilder.group({
        password: ['', [Validators.required, Validators.minLength(6), Validators.pattern('(?=.*\\d)(?=.*[0-9])(?=.*[a-záéíóúüñ]).*[A-ZÁÉÍÓÚÜÑ].*')]],
        passwordConfirmation: ['', [Validators.required, Validators.minLength(6), this.matchOtherValidator('password')]]
      }),
      termsAndConditions: [false, [Validators.requiredTrue]],
    });
  }
  // Función que mostrara un texto al hacer click o enter en correo
  async presentToast1() {
    const toast = await this.toastController.create({
      message: 'Su correo eléctronico sera usado como usuario en el inicio de sesión.',
      duration: 4000,
      position: 'bottom'
    });
    toast.present();
  }
  Event(keyCode) {
    if (keyCode === 13) {
      this.presentToast1();
    }
  }
  async popUp() {
    const modal = this.modalCtrl.create({
      component: EmailVerificationPage,
      cssClass: 'my-emailv-css',
    });
    return (await modal).present()
  }
}