import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ModalController } from '@ionic/angular';
import { AuthService } from 'src/app/core/service/auth.service';
import { FirestoreCRUDService } from 'src/app/core/service/firestore/firestore-crud.service';
import { LogicReuseService } from 'src/app/shared/resources/logic-reuse.service';

@Component({
  selector: 'app-email-verification',
  templateUrl: './email-verification.page.html',
  styleUrls: ['./email-verification.page.scss'],
})
export class EmailVerificationPage implements OnInit {

  // email: string;
  public emailUser = this.authService.emailUser;

  constructor(
    public authService: AuthService, private logicReuse: LogicReuseService, private modalCtrl: ModalController
  ) { }

  ngOnInit() {
  }

  sendEmail() {
    this.authService.sendEmailVerification();
    this.logicReuse.presentToastSuccess('<ion-icon name="checkmark-circle"></ion-icon> EL correo electrónico de verificación se envío correctamente.');
  }

  cerrarModal() {
    this.modalCtrl.dismiss();
  }

}
