import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ABOUTUSPage } from './aboutus.page';

const routes: Routes = [
  {
    path: '',
    component: ABOUTUSPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ABOUTUSPageRoutingModule {}
