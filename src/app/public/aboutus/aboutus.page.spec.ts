import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ABOUTUSPage } from './aboutus.page';

describe('ABOUTUSPage', () => {
  let component: ABOUTUSPage;
  let fixture: ComponentFixture<ABOUTUSPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ABOUTUSPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ABOUTUSPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
