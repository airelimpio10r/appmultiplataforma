import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { IonContent, ModalController, ToastController } from '@ionic/angular';
import { VideolabService } from 'src/app/core/service/videolab.service';
import { VideoPopupPage } from '../video-popup/video-popup.page';
import { ModalImagePage } from '../bankofprojects/projectsdetails/modal-image/modal-image.page';
import { AuthService } from 'src/app/core/service/auth.service';
import { FirestoreCRUDService } from 'src/app/core/service/firestore/firestore-crud.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { LogicReuseService } from 'src/app/shared/resources/logic-reuse.service';
import { EmailVerificationPage } from '../authentication/email-verification/email-verification.page';
import { Pqr } from 'src/app/shared/pqr.interface';

@Component({
  selector: 'app-home',
  templateUrl: './home.page.html',
  styleUrls: ['./home.page.scss'],
})
export class HomePage implements OnInit {

  date: Date = new Date();
  items = [];
  items2 = [];
  // Activa los slides
  slideOpts = {
    initialSlide: 1,
    speed: 400,
    autoplay: true
  };
  active: boolean;
  // Scroll bar movement
  @ViewChild(IonContent, { static: false }) content: IonContent;
  FormContactanos: FormGroup;
  validation_messages;
  inputUntouched = false;

  constructor(
    public authService: AuthService, public router: Router,
    private toastController: ToastController,
    private modalvideo: ModalController, private videoservice: VideolabService,
    private modalCtrl: ModalController, private CRUD: FirestoreCRUDService, private logicReuse: LogicReuseService,
    private formBuilder: FormBuilder,
  ) { }

  ngOnInit() {
    this.items = this.videoservice.getvideo();
    this.items2 = this.videoservice.getvideo2();
    this.hiddenHTML();
    this.FormContactanos = this.FormValid();
    this.validation_messages = this.logicReuse.validation_messages;
  }
  // Envia un PQR
  sendEmail(value) {
    if (this.FormContactanos.valid) {
      var randomId = Math.random().toString(36).substring(2, 8);
      const data: Pqr = {
        radicado: randomId,
        email: value.email,
        pqr: value.bodymessage,
        date: this.date,
        estado: false
      }
      this.CRUD.savePQRSD(data);
      this.FormContactanos.reset();
      this.logicReuse.presentToastSuccess('<ion-icon name="checkmark-circle"></ion-icon> Su mensaje fue enviado con éxito y será respondido en brevedad, por favor este pendiente del correo electrónico ingresado.');
      this.inputUntouched = false;
    } else {
      this.inputUntouched = true;
    }
  }
  //Evalua el uso del botton enter para hacer el submit
  eventHandler(keyCode) {
    if (keyCode === 13) {
      if (this.FormContactanos.valid) {
        // Trigger the button element with a click
        document.getElementById("bttSubmitContactanos").click();
      }
    }
  }
  // Condiciones de la entrada de datos form
  private FormValid() {
    return this.formBuilder.group({
      email: ['', [Validators.required, Validators.email]],
      bodymessage: ['', [Validators.required, Validators.maxLength(1600), Validators.minLength(2)]]
    })
  }
  //Movimiento del Scroll
  scrollMovement() {
    var titleELe = document.getElementById('foot');
    this.content.scrollToPoint(0, titleELe.offsetTop, 1000);
  }
  //Esconde partes del html cuando esta logiado
  hiddenHTML() {
    this.authService.user$.subscribe(data => {
      if (data) {
        this.active = true;
      } else {
        this.active = false;
      }
    })
  }
  // Carga una imagen y video en una page como popup
  async popUp() {
    const modal = this.modalCtrl.create({
      component: ModalImagePage,
      cssClass: 'my-modal-css',
    });
    return (await modal).present()
  }
  async videomodal(value: any) {
    const modal = this.modalvideo.create({
      component: VideoPopupPage,
      cssClass: 'my-modal-css',
      componentProps: {
        passurl: value
      }
    });
    return (await modal).present()
  }

  //toast iOs
  async presentToastApple() {
    const toast = await this.toastController.create({
      header: 'Para descargar la aplicación en dispositivos Apple',
      message: 'Comuníquese con el equipo de aire+limpio con el siguiente correo 10airelimpio@gmail.com',
      position: 'middle',
      // color:'success',
      cssClass: "my-custom-class",
      buttons: [{
        text: 'Entendido',
        role: 'cancel',
        handler: () => {
          console.log('Cancel clicked');
        }
      }
      ]
    });
    toast.present();
  }

}