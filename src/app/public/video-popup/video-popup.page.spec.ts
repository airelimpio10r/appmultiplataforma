import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { VideoPopupPage } from './video-popup.page';

describe('VideoPopupPage', () => {
  let component: VideoPopupPage;
  let fixture: ComponentFixture<VideoPopupPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VideoPopupPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(VideoPopupPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
