import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { VideoPopupPageRoutingModule } from './video-popup-routing.module';

// import { VideoPopupPage } from './video-popup.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    VideoPopupPageRoutingModule
  ],
  // declarations: [VideoPopupPage]
})
export class VideoPopupPageModule {}
