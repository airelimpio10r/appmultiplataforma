import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import { IonContent } from '@ionic/angular';
import { LogicReuseService } from 'src/app/shared/resources/logic-reuse.service';

@Component({
  selector: 'app-plan-accion',
  templateUrl: './plan-accion.page.html',
  styleUrls: ['./plan-accion.page.scss'],
})
export class PlanAccionPage implements OnInit {

  // ID = this.logicReuse.scrollNumber
  // Scroll bar movement
  @ViewChild(IonContent, { static: false }) content: IonContent;

  constructor(
    private logicReuse: LogicReuseService
  ) {
  }

  ngOnInit() {
  }

  ngAfterViewChecked() {
    var value = this.logicReuse.scrollNumber
    if (value == 1) {
      var titleELe = document.getElementById('1');
      this.content.scrollToPoint(0, titleELe.offsetTop, 1000);
    }
    else if (value == 2) {
      var titleELe = document.getElementById('2');
      this.content.scrollToPoint(0, titleELe.offsetTop, 1000);
    } else if (value == 3) {
      var titleELe = document.getElementById('3');
      this.content.scrollToPoint(0, titleELe.offsetTop, 1000);
    } else if (value == 4) {
      var titleELe = document.getElementById('4');
      this.content.scrollToPoint(0, titleELe.offsetTop, 1000);
    } else if (value == 5) {
      var titleELe = document.getElementById('5');
      this.content.scrollToPoint(0, titleELe.offsetTop, 1000);
    } else if (value == 6) {
      var titleELe = document.getElementById('6');
      this.content.scrollToPoint(0, titleELe.offsetTop, 1000);
    } 
    else if (value == 12) {
      var titleELe = document.getElementById('12');
      this.content.scrollToPoint(0, titleELe.offsetTop, 1000);
    } else if (value == 13) {
      var titleELe = document.getElementById('13');
      this.content.scrollToPoint(0, titleELe.offsetTop, 1000);
    } else if (value == 14) {
      var titleELe = document.getElementById('14');
      this.content.scrollToPoint(0, titleELe.offsetTop, 1000);
    }
  }

}
