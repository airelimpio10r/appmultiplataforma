import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { PlanAccionPage } from './plan-accion.page';

describe('PlanAccionPage', () => {
  let component: PlanAccionPage;
  let fixture: ComponentFixture<PlanAccionPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PlanAccionPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(PlanAccionPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
