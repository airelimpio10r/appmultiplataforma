import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PlanAccionPage } from './plan-accion.page';

const routes: Routes = [
  {
    path: '',
    component: PlanAccionPage
  },

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PlanAccionPageRoutingModule {}
