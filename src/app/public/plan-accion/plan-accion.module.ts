import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { PlanAccionPageRoutingModule } from './plan-accion-routing.module';

import { PlanAccionPage } from './plan-accion.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    PlanAccionPageRoutingModule
  ],
  declarations: [PlanAccionPage]
})
export class PlanAccionPageModule {}
