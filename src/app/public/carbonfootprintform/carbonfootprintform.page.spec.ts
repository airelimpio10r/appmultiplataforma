import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { CarbonfootprintformPage } from './carbonfootprintform.page';

describe('CarbonfootprintformPage', () => {
  let component: CarbonfootprintformPage;
  let fixture: ComponentFixture<CarbonfootprintformPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CarbonfootprintformPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(CarbonfootprintformPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
