import { Component, OnInit, ViewChild, ChangeDetectorRef, Input } from '@angular/core';
import { IonSlides, ModalController } from '@ionic/angular';
import { Router } from '@angular/router';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { LogicReuseService } from 'src/app/shared/resources/logic-reuse.service';
import { empty } from 'rxjs';
import { ModalImagePage } from '../bankofprojects/projectsdetails/modal-image/modal-image.page';
import { Porcentages } from 'src/app/shared/patrocinio.interface';


@Component({
  selector: 'app-carbonfootprintform',
  templateUrl: './carbonfootprintform.page.html',
  styleUrls: ['./carbonfootprintform.page.scss'],
})
export class CarbonfootprintformPage implements OnInit {
  @Input('progress') progress;
  CalculoHuellaCarbono: number;
  CalculodeArboles: number;
  CalculodeElectricidad: number;
  CalculodeAlimentacion: number;
  CalculodeTransporteTerrestre: number;
  CalculodeTransporteAereo: number;
  //Forms
  carbonFootprint: FormGroup;
  //Booleanos
  activeElectric: boolean;
  activeAlimt: boolean;
  activeAvion: boolean;
  disableElectric: boolean;
  disableAlimt: boolean;
  disableTransporeHora: boolean;
  disableTransporeKM: boolean;
  disableAvion: boolean;
  //Booleanos
  cambiarFondo: string = 'cero';
  ocultarSectionCalcular: boolean = true;
  ocultarSectionElect: boolean;
  ocultarSectionFood: boolean;
  ocultarSectionTrans: boolean;
  ocultarSectionPlane: boolean;
  ocultarSectionResult: boolean;

  constructor(
    private formBuilder: FormBuilder, private router: Router,
    private logicReuse: LogicReuseService, private modalCtrl: ModalController
  ) { }

  ngOnInit() {
    this.carbonFootprint = this.FormcarbonFootprint();
  }
  // Carga una imagen y video en una page como popup
  async popUp() {
    const modal = this.modalCtrl.create({
      component: ModalImagePage,
      cssClass: 'my-modal-css',
    });
    return (await modal).present()
  }
  //Avanzar entre slides
  nextSlide1() {
    this.ocultarSectionCalcular = !this.ocultarSectionCalcular;
    this.ocultarSectionElect = !this.ocultarSectionElect;
    this.cambiarFondo = 'first'
  }
  nextSlideElecticidad() {
    if (this.carbonFootprint.value.sabeConsumoElect == true) {
      if (this.carbonFootprint.value.valorElectrico == '' || this.carbonFootprint.value.Npersonas == '' || this.carbonFootprint.value.estrato == '') {
        this.inputUntouched = true;
        this.logicReuse.presentToast(' <ion-icon name="alert-circle-outline"></ion-icon> Por favor, llene todos los campos del formulario de la huella de carbono.');
      } else {
        this.ocultarSectionElect = !this.ocultarSectionElect;
        this.ocultarSectionFood = !this.ocultarSectionFood;
        this.cambiarFondo = 'second';
        this.inputUntouched = false;
      }
    } else if (this.carbonFootprint.value.noSabeConsumoElect == true) {
      this.ocultarSectionElect = !this.ocultarSectionElect;
      this.ocultarSectionFood = !this.ocultarSectionFood;
      this.cambiarFondo = 'second';
      this.inputUntouched = false;
    } else {
      this.logicReuse.presentToast(' <ion-icon name="alert-circle-outline"></ion-icon> Por favor, llene todos los campos del formulario de la huella de carbono.');
    }
  }
  nextSlideAlimento() {
    if (this.carbonFootprint.value.dietaAlimen == true) {
      if (this.carbonFootprint.value.tuDieta == '') {
        this.logicReuse.presentToast(' <ion-icon name="alert-circle-outline"></ion-icon> Por favor, llene todos los campos del formulario de la huella de carbono.');
      } else {
        this.ocultarSectionFood = !this.ocultarSectionFood;
        this.ocultarSectionTrans = !this.ocultarSectionTrans;
        this.cambiarFondo = 'third';
        this.inputUntouched = false;
      }
    } else if (this.carbonFootprint.value.consumoalimen == true) {
      if (this.carbonFootprint.value.res == '' || this.carbonFootprint.value.cerveza == '' || this.carbonFootprint.value.pollo == '' || this.carbonFootprint.value.arroz == '' || this.carbonFootprint.value.chocolate == '') {
        this.inputUntouched = true;
        this.logicReuse.presentToast(' <ion-icon name="alert-circle-outline"></ion-icon> Por favor, llene todos los campos del formulario de la huella de carbono.');
      } else {
        this.ocultarSectionFood = !this.ocultarSectionFood;
        this.ocultarSectionTrans = !this.ocultarSectionTrans;
        this.cambiarFondo = 'third';
        this.inputUntouched = false;
      }
    } else {
      this.logicReuse.presentToast(' <ion-icon name="alert-circle-outline"></ion-icon> Por favor, llene todos los campos del formulario de la huella de carbono.');
    }
  }
  nextSlideTransporte() {
    if (this.carbonFootprint.value.MedioDeTrasporte == '') {
      this.logicReuse.presentToast(' <ion-icon name="alert-circle-outline"></ion-icon> Por favor, llene todos los campos del formulario de la huella de carbono.');
    } else if (this.carbonFootprint.value.distanciaHoras == true) {
      if (this.carbonFootprint.value.horasdeManejo == '') {
        this.logicReuse.presentToast(' <ion-icon name="alert-circle-outline"></ion-icon> Por favor, llene todos los campos del formulario de la huella de carbono.');
        this.inputUntouched = true;
      } else {
        this.ocultarSectionTrans = !this.ocultarSectionTrans;
        this.ocultarSectionPlane = !this.ocultarSectionPlane;
        this.cambiarFondo = 'four';
        this.inputUntouched = false;
      }
    } else if (this.carbonFootprint.value.distanciaKM == true) {
      if (this.carbonFootprint.value.distanciaDiaria == '') {
        this.logicReuse.presentToast(' <ion-icon name="alert-circle-outline"></ion-icon> Por favor, llene todos los campos del formulario de la huella de carbono.');
        this.inputUntouched = true;
      } else {
        this.ocultarSectionTrans = !this.ocultarSectionTrans;
        this.ocultarSectionPlane = !this.ocultarSectionPlane;
        this.cambiarFondo = 'four';
        this.inputUntouched = false;
      }
    } else {
      this.logicReuse.presentToast(' <ion-icon name="alert-circle-outline"></ion-icon> Por favor, llene todos los campos del formulario de la huella de carbono.');
    }
  }
  // Pasa el slide
  behindSlide() {
    if (this.ocultarSectionElect == true) {
      this.ocultarSectionElect = !this.ocultarSectionElect;
      this.ocultarSectionCalcular = !this.ocultarSectionCalcular;
      this.cambiarFondo = 'cero';
    } else if (this.ocultarSectionFood == true) {
      this.ocultarSectionFood = !this.ocultarSectionFood;
      this.ocultarSectionElect = !this.ocultarSectionElect;
      this.cambiarFondo = 'first'
    } else if (this.ocultarSectionTrans == true) {
      this.ocultarSectionTrans = !this.ocultarSectionTrans;
      this.ocultarSectionFood = !this.ocultarSectionFood;
      this.cambiarFondo = 'second'
    } else if (this.ocultarSectionPlane == true) {
      this.ocultarSectionPlane = !this.ocultarSectionPlane;
      this.ocultarSectionTrans = !this.ocultarSectionTrans;
      this.cambiarFondo = 'third'
    }
  }
  // Reiniciar el formulario
  restarForm() {
    this.ocultarSectionCalcular = !this.ocultarSectionCalcular;
    this.ocultarSectionResult = !this.ocultarSectionResult;
    this.disableElectric = false;
    this.disableAlimt = false;
    this.disableTransporeHora = false;
    this.disableTransporeKM = false;
    this.disableAvion = false;
    this.activeElectric = false;
    this.activeAlimt = false;
    this.activeAvion = false;
    this.CalculoHuellaCarbono = 0;
    this.carbonFootprint.reset();
    this.inputUntouched = false;
    this.carbonFootprint = this.FormcarbonFootprint();
  }
  // esta funcion va anclada con el formulario
  calculateFootprint(value) {
    var electricidad: number = 0;
    var alimentación: number = 0;
    var transporteTerrestre: number = 0;
    var transporteAereo: number = 0;
    var calculo: number = 0;
    //calculo electricidad
    if (value.sabeConsumoElect == true) {
      electricidad = ((parseFloat(value.valorElectrico) / value.estrato) * 0.005724) / parseFloat(value.Npersonas);
    } else {
      electricidad = 0.3897;
    }
    calculo = electricidad;
    //calculo Alimentos
    if (value.dietaAlimen == true) {
      alimentación = parseFloat(value.tuDieta);
    } else if (value.consumoalimen == true) {
      var valorcomida = (((value.cordero * 0.039) + (value.res * 0.027) + (value.queso * 0.013) + (value.tocino * 0.0125) + (value.cerdo * 0.012) + (value.camarones * 0.012) + (value.pollo * 0.004) +
        (value.huevos * 0.004) + (value.arroz * 0.0029)) + (value.chocolate * 0.011) + (value.cafe * 0.0079) + (value.vino * 0.003) + (value.leche * 0.0012) + (value.cerveza * 0.0012));
      alimentación = valorcomida;
    }
    calculo += alimentación;
    //calculo transporte terrestre
    if (value.distanciaKM == true) {
      transporteTerrestre = 0.10656 * value.distanciaDiaria;
    } else {
      transporteTerrestre = value.MedioDeTrasporte * value.horasdeManejo;
    }
    calculo += transporteTerrestre;
    //calculo transporte Aereo
    if (isNaN(calculo)) {
      this.logicReuse.presentToast(' <ion-icon name="alert-circle-outline"></ion-icon> Por favor, llene todos los campos del formulario de la huella de carbono.');
    } else if (this.carbonFootprint.value.avionSi == true) {
      if (this.carbonFootprint.value.nacional == '') {
        this.logicReuse.presentToast(' <ion-icon name="alert-circle-outline"></ion-icon> Por favor, llene todos los campos del formulario de la huella de carbono.');
        this.inputUntouched = true;
      } else {
        transporteAereo = ((value.nacional * 0.22500711) + (value.norteamerica * 2.290374) + (value.suramerica * 1.989009) + (value.europa * 4.520475) + (value.asia * 7.534125) + (value.oceania * 7.534125) +
          (value.africa * 7.534125));
        calculo += transporteAereo;
        // para reondear el numero hacia abajo con Math.floor(number); y para que cresca con Math.round(number) y para quitar desimales con Math.trunc(number)
        this.CalculoHuellaCarbono = (Math.trunc(calculo * 100)) / 100;
        this.logicReuse.carbonFootprintCalculation = this.CalculoHuellaCarbono;
        this.CalculodeArboles = Math.round(2.5001 * (this.CalculoHuellaCarbono) - 0.0445);
        //NextSlide
        this.ocultarSectionPlane = !this.ocultarSectionPlane;
        this.ocultarSectionResult = !this.ocultarSectionResult;
        this.cambiarFondo = 'five';
        this.inputUntouched = false;
      }
    } else if (this.carbonFootprint.value.avionNo == true) {
      // para reondear el numero hacia abajo con Math.floor(number); y para que cresca con Math.round(number) y para quitar desimales con Math.trunc(number)
      this.CalculoHuellaCarbono = (Math.trunc(calculo * 100)) / 100;
      this.logicReuse.carbonFootprintCalculation = this.CalculoHuellaCarbono;
      this.CalculodeArboles = Math.round(2.5001 * (this.CalculoHuellaCarbono) - 0.0445);
      this.ocultarSectionPlane = !this.ocultarSectionPlane;
      this.ocultarSectionResult = !this.ocultarSectionResult;
      this.cambiarFondo = 'five';
      this.inputUntouched = false;
    } else {
      this.logicReuse.presentToast(' <ion-icon name="alert-circle-outline"></ion-icon> Por favor, llene todos los campos del formulario de la huella de carbono.');
    }
    // calculo de los porcentajes par ala grafica
    this.CalculodeElectricidad = (Math.trunc(((electricidad * 100) / this.CalculoHuellaCarbono) * 100) / 100);
    this.CalculodeAlimentacion = (Math.trunc(((alimentación * 100) / this.CalculoHuellaCarbono) * 100) / 100);
    this.CalculodeTransporteTerrestre = (Math.trunc(((transporteTerrestre * 100) / this.CalculoHuellaCarbono) * 100) / 100);
    this.CalculodeTransporteAereo = (Math.trunc(((transporteAereo * 100) / this.CalculoHuellaCarbono) * 100) / 100);

    //Carga los datos apra enviarlos a la pagina de aportes(sponsorshit)
    this.logicReuse.carbonFootprintPorcentages = {
      CalculodeElectricidad: this.CalculodeElectricidad,
      CalculodeAlimentacion: this.CalculodeAlimentacion,
      CalculodeTransporteTerrestre: this.CalculodeTransporteTerrestre,
      CalculodeTransporteAereo: this.CalculodeTransporteAereo,
    }
  }
  // Condiciones de la entrada de datos form
  private FormcarbonFootprint() {
    return this.formBuilder.group({
      // Electricidad form
      estrato: ['',],
      valorElectrico: [''],
      Npersonas: ['', [Validators.required, Validators.pattern('[1-9]*'), Validators.min(1)]],
      // Comida form
      tuDieta: [''],
      cordero: ['', [Validators.required, Validators.pattern('[0-9]*'), Validators.min(1)]],
      res: ['', [Validators.required, Validators.pattern('[0-9]*'), Validators.min(1)]],
      queso: ['', [Validators.required, Validators.pattern('[0-9]*'), Validators.min(1)]],
      tocino: ['', [Validators.required, Validators.pattern('[0-9]*'), Validators.min(1)]],
      cerdo: ['', [Validators.required, Validators.pattern('[0-9]*'), Validators.min(1)]],
      camarones: ['', [Validators.required, Validators.pattern('[0-9]*'), Validators.min(1)]],
      pollo: ['', [Validators.required, Validators.pattern('[0-9]*'), Validators.min(1)]],
      huevos: ['', [Validators.required, Validators.pattern('[0-9]*'), Validators.min(1)]],
      arroz: ['', [Validators.required, Validators.pattern('[0-9]*'), Validators.min(1)]],
      chocolate: ['', [Validators.required, Validators.pattern('[0-9]*'), Validators.min(1)]],
      cafe: ['', [Validators.required, Validators.pattern('[0-9]*'), Validators.min(1)]],
      vino: ['', [Validators.required, Validators.pattern('[0-9]*'), Validators.min(1)]],
      leche: ['', [Validators.required, Validators.pattern('[0-9]*'), Validators.min(1)]],
      cerveza: ['', [Validators.required, Validators.pattern('[0-9]*'), Validators.min(1)]],
      //Terrestre
      distanciaDiaria: ['', [Validators.required, Validators.pattern('[1-9]*'), Validators.min(1)]],
      horasdeManejo: ['', [Validators.required, Validators.pattern('[1-9]*'), Validators.min(1)]],
      MedioDeTrasporte: [''],
      //Aereo
      nacional: ['', [Validators.required, Validators.pattern('[0-9]*'), Validators.min(1)]],
      norteamerica: ['', [Validators.required, Validators.pattern('[0-9]*'), Validators.min(1)]],
      suramerica: ['', [Validators.required, Validators.pattern('[0-9]*'), Validators.min(1)]],
      europa: ['', [Validators.required, Validators.pattern('[0-9]*'), Validators.min(1)]],
      asia: ['', [Validators.required, Validators.pattern('[0-9]*'), Validators.min(1)]],
      oceania: ['', [Validators.required, Validators.pattern('[0-9]*'), Validators.min(1)]],
      africa: ['', [Validators.required, Validators.pattern('[0-9]*'), Validators.min(1)]],
      //Checkbox
      sabeConsumoElect: [false],
      noSabeConsumoElect: [false],
      consumoalimen: [false],
      dietaAlimen: [false],
      distanciaKM: [false],
      distanciaHoras: [false],
      avionSi: [false],
      avionNo: [false],
    })
  }

  //Mensajes de error
  inputUntouched: boolean = false;
  validation_messages = {
    'Npersonas': [
      { type: 'required', message: 'La cantidad de personas es requerida.' },
      { type: 'pattern', message: 'Por favor, ingrese un número válido.' },
    ],
    'cordero': [
      { type: 'required', message: 'La cantidad de cordero es requerida.' },
      { type: 'pattern', message: 'Por favor, ingrese un número válido.' },
    ],
    'res': [
      { type: 'required', message: 'La cantidad de carne de res es requerida.' },
      { type: 'pattern', message: 'Por favor, ingrese un número válido.' },
    ],
    'queso': [
      { type: 'required', message: 'La cantidad de queso es requerida.' },
      { type: 'pattern', message: 'Por favor, ingrese un número válido.' },
    ],
    'tocino': [
      { type: 'required', message: 'La cantidad de tocino es requerida.' },
      { type: 'pattern', message: 'Por favor, ingrese un número válido.' },
    ],
    'cerdo': [
      { type: 'required', message: 'La cantidad de cerdo es requerida.' },
      { type: 'pattern', message: 'Por favor, ingrese un número válido.' },
    ],
    'camarones': [
      { type: 'required', message: 'La cantidad de camarones es requerida.' },
      { type: 'pattern', message: 'Por favor, ingrese un número válido.' },
    ],
    'pollo': [
      { type: 'required', message: 'La cantidad de pollo es requerida.' },
      { type: 'pattern', message: 'Por favor, ingrese un número válido.' },
    ],
    'huevos': [
      { type: 'required', message: 'La cantidad de huevos es requerida.' },
      { type: 'pattern', message: 'Por favor, ingrese un número válido.' },
    ],
    'arroz': [
      { type: 'required', message: 'La cantidad de arroz es requerida.' },
      { type: 'pattern', message: 'Por favor, ingrese un número válido.' },
    ],
    'chocolate': [
      { type: 'required', message: 'La cantidad de chocolate es requerida.' },
      { type: 'pattern', message: 'Por favor, ingrese un número válido.' },
    ],
    'cafe': [
      { type: 'required', message: 'La cantidad de café es requerida.' },
      { type: 'pattern', message: 'Por favor, ingrese un número válido.' },
    ],
    'vino': [
      { type: 'required', message: 'La cantidad de vino es requerida.' },
      { type: 'pattern', message: 'Por favor, ingrese un número válido.' },
    ],
    'leche': [
      { type: 'required', message: 'La cantidad de leche es requerida.' },
      { type: 'pattern', message: 'Por favor, ingrese un número válido.' },
    ],
    'cerveza': [
      { type: 'required', message: 'La cantidad de cerveza es requerida.' },
      { type: 'pattern', message: 'Por favor, ingrese un número válido.' },
    ],
    // 'ChooseVehi': [
    //   { type: 'required', message: 'Es requerido que seleccione el vehículo.' },
    // ],

    'distanciaDiaria': [
      { type: 'required', message: 'La distancia diaria recorrida es requerida.' },
      { type: 'pattern', message: 'Por favor, ingrese un número válido.' },
    ],
    'horasdeManejo': [
      { type: 'required', message: 'La cantidad de horas de manejo es requerida.' },
      { type: 'pattern', message: 'Por favor, ingrese un número válido.' },
    ],
    'nacional': [
      { type: 'required', message: 'La cantidad de vuelos Nacionales es requerida.' },
      { type: 'pattern', message: 'Por favor, ingrese un número válido.' },
    ],
    'norteamerica': [
      { type: 'required', message: 'La cantidad de vuelos a Norteamerica es requerida.' },
      { type: 'pattern', message: 'Por favor, ingrese un número válido.' },
    ],
    'suramerica': [
      { type: 'required', message: 'La cantidad de vuelos a Suramerica es requerida.' },
      { type: 'pattern', message: 'Por favor, ingrese un número válido.' },
    ],
    'europa': [
      { type: 'required', message: 'La cantidad de vuelos a Europa es requerida.' },
      { type: 'pattern', message: 'Por favor, ingrese un número válido.' },
    ],
    'asia': [
      { type: 'required', message: 'La cantidad de vuelos a Asia es requerida.' },
      { type: 'pattern', message: 'Por favor, ingrese un número válido.' },
    ],
    'oceania': [
      { type: 'required', message: 'La cantidad de vuelos a Oceanía es requerida.' },
      { type: 'pattern', message: 'Por favor, ingrese un número válido.' },
    ],
    'africa': [
      { type: 'required', message: 'La cantidad de vuelos a África es requerida.' },
      { type: 'pattern', message: 'Por favor, ingrese un número válido.' },
    ],
  }

}
