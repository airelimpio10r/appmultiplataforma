import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { ModalController, Platform } from '@ionic/angular';
import { ModalGaleryPage } from '../bankofprojects/projectsdetails/modal-galery/modal-galery.page';

@Component({
  selector: 'app-benefits',
  templateUrl: './benefits.page.html',
  styleUrls: ['./benefits.page.scss'],
})
export class BenefitsPage implements OnInit {
  // images=['1.jpg','2.jpg','3.jpg'];


  // Activa los slides
  // slideOpts = {

  //   zoom:false,
  //   spaceBetween:10,
  //   slidesPerView:1.5,
  // };
  // public devWidth = this.platform.width();
  // public data = [
  //   {
  //     imgs: [
  //       'assets/images/1.jpg',
  //       'assets/images/2.jpg',
  //       'assets/images/3.jpg',
  //       "https://images.pexels.com/photos/1707402/pexels-photo-1707402.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940"
  //     ]
  //   }
  // ];
  // @ViewChild("slider", { read: ElementRef, static: false }) slider: ElementRef;
  // sliderOpts = {
  //   initialSlide: 0,
  //   slidesPerView: 1,
  //   loopedSlides: 5
  // };

  // sliderOpts2 = {
  //   initialSlide: 0,
  //   slidesPerView: 1,
  //   loop: true
  // };
  constructor(
    // private modalCtrl: ModalController,  private platform: Platform
  ) {

  }

  ngOnInit() {
  }

  // slideNext(slideView) {
  //   slideView.slideNext(500);
  // }

  // slidePrev(slideView) {
  //   slideView.slidePrev(500);
  // }

  // slideTo(index) {
  //   this.slider.nativeElement.slideTo(index);
  // }

  // openPreview(img) {
  //   this.modalCtrl
  //     .create({
  //       component: ModalGaleryPage,
  //       componentProps: {
  //         imgURL: this.data[0].imgs
  //       },
  //       cssClass: "my-custom-modal-css"
  //     })
  //     .then(modal => modal.present());

  //   if (!window.history.state.modal) {
  //     const modalState = { modal: true };
  //     history.pushState(modalState, null);
  //   }
  // }
  // OpenPreview(img){
  //   const modal = this.modalCtrl.create({
  //     component: ModalGaleryPage,
  //   }).then(modal=>modal.present());
  // }
  // async popUpGalery(img) {
  //   const modal = this.modalCtrl.create({
  //     component: ModalGaleryPage,
  //     componentProps:{
  //       img:img
  //     },
  //     cssClass: 'my-modal-galery-css',
  //   });
  //   return (await modal).present()
  // }
  // openPreviewMobile(img) {
  //   this.modalCtrl
  //     .create({
  //       component: ModalGaleryPage,
  //       componentProps: {
  //         imgURL: this.data[0].imgs
  //       }
  //     })
  //     .then(modal => modal.present());
  //   if (!window.history.state.modal) {
  //     const modalState = { modal: true };
  //     history.pushState(modalState, null);
  //   }
  // }
}
