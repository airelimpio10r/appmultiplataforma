import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';

import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';

//Agregamos los componentes
import { FormsModule, ReactiveFormsModule } from '@angular/forms'; // Componentes de la propiedad Form
//Firebase
import { AngularFireDatabaseModule, AngularFireDatabase } from "@angular/fire/database";
import { AngularFireAuthModule } from "@angular/fire/auth"; 
import { AngularFireModule } from "@angular/fire"; 
import { AngularFirestoreModule } from '@angular/fire/firestore';
import { AngularFireStorageModule } from '@angular/fire/storage';
//Base de datos local
import { IonicStorageModule } from '@ionic/storage';

import { AuthGuard } from './core/guards/auth.guard'
import { VideoPopupPage } from './public/video-popup/video-popup.page';
import { ModalImagePageModule } from './public/bankofprojects/projectsdetails/modal-image/modal-image.module';
import { ModalGaleryPageModule } from './public/bankofprojects/projectsdetails/modal-galery/modal-galery.module';
import { firebaseConfig } from 'src/environments/environment.prod';
import { ServiceWorkerModule } from '@angular/service-worker';
import { environment } from '../environments/environment';

@NgModule({
  declarations: [AppComponent,VideoPopupPage],
  entryComponents: [VideoPopupPage],
  imports: [
    BrowserModule,
    IonicModule.forRoot(),
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    AngularFireModule.initializeApp(firebaseConfig),
    AngularFirestoreModule,
    AngularFireAuthModule,
    AngularFireDatabaseModule,
    AngularFireStorageModule,
    ModalImagePageModule,
    ModalGaleryPageModule,
    IonicStorageModule.forRoot(),
    ServiceWorkerModule.register('ngsw-worker.js', { enabled: environment.production }),
  ],
  providers: [
    StatusBar,
    SplashScreen,
    AuthGuard,
    AngularFireDatabase,
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy },
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
