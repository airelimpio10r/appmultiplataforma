//Datos basicos del usuario registrado
export interface User {
    name?: string;
    lastName?: String;
    password?: String;
    photoProfile?: PhotoProfile;
    email?: string;
    nit?: string;
    companyName?: string;
    companyActivity?: string;
    documentType?: string;
    document?: Number;
    cellPhone?: Number;
    direction?: string;
    rol?: string;
    country?: string;
    department?: string;
    municipality?: string;
    telephone?: Number;
    date?: Date;
}
export interface PhotoProfile{
    photoURL?: string;
    uidPhoto?: string;
}
export interface Authen{
    displayName?: string;
    email?: string;
    emailVerified?: boolean;
    phoneNumber?: string;
    photoURL?: string;
}