export interface RegisterProject {
    projectType?: string;
    projectName?: string;
    forestArea?: Number;
    forestType?: string;
    temperature?: Number;
    typeTemperature?: string;
    treeDiameter?: string;
    treeAge?: string;
    treeSize?: string;
    country?: string;
    department?: string;
    municipality?: string;
    zone?: string;
    msnm?: string;
    propertyName?: string;
    cadastralCertificate?: string;
    enrollment?: string;
    treeSpecies?: Array<string>;
    user?: User;
    photos?: IType [];
    date?: Date;
    Validate?: string;
    RTAValidacion?: string;
}
export interface IType {
    [key: string]: string;
}
export interface User {
    email: string;
}