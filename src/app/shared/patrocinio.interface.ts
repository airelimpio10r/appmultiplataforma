export interface Sponsor {
    id: string;
    carbonfootprint?: number;
    amountMoney: number;
    annualPrice: number;
    contributionFrequency?: string;
    user?: User;
    date?: Date; 
    name?: string;
    lastName?: String;
    cellPhone?: Number;
    carbonPorcentages:Porcentages;
}

export interface Porcentages{
    CalculodeElectricidad: number;
    CalculodeAlimentacion: number;
    CalculodeTransporteTerrestre: number;
    CalculodeTransporteAereo: number;
}

export interface User {
    email: string;
}