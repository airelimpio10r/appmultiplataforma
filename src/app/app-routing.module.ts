import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import { AuthGuard } from '../app/core/guards/auth.guard';
import { NoLoginGuard } from '../app/core/guards/no-login.guard'
import { CheckTutorialGuard } from './core/guards/check-tutorial.guard';

const routes: Routes = [
  {
    path: '',
    // Modificando esto se puede cambiar la pagina que aparece despues de la carga inicial
    redirectTo: 'tutorial',
    pathMatch: 'full'
  },
  {
    path: 'home',
    loadChildren: () => import('./public/home/home.module').then( m => m.HomePageModule)
  },
  {
    path: 'login',
    children:[
      {
        path: '',
        loadChildren: () => import('./public/authentication/login/login.module').then( m => m.LoginPageModule),
        canActivate:[NoLoginGuard]
      },
      {
        path: 'registry',
        loadChildren: () => import('./public/authentication/registry/registry.module').then( m => m.RegistryPageModule),
        canActivate:[NoLoginGuard],
      },
      {
        path: 'recoverpassword',
        loadChildren: () => import('./public/authentication/recoverpassword/recoverpassword.module').then( m => m.RecoverpasswordPageModule),
        canActivate:[NoLoginGuard],
      },
      {
        path: 'email-verification',
        loadChildren: () => import('./public/authentication/email-verification/email-verification.module').then( m => m.EmailVerificationPageModule),
        canActivate:[NoLoginGuard],
      }
    ]
  },
  {
    path: 'aboutus',
    loadChildren: () => import('./public/aboutus/aboutus.module').then( m => m.ABOUTUSPageModule)
  },
  {
    path: 'carbonfootprintform',
    loadChildren: () => import('./public/carbonfootprintform/carbonfootprintform.module').then( m => m.CarbonfootprintformPageModule)
  },
  {
    path: 'projectregistrationform',
    loadChildren: () => import('./private/projectregistrationform/projectregistrationform.module').then( m => m.ProjectregistrationformPageModule),
    canActivate:[AuthGuard],
  },
  {
    path: 'sponsorshipform',
    loadChildren: () => import('./private/sponsorshipform/sponsorshipform.module').then( m => m.SponsorshipformPageModule),
  },
  {
    path: 'account',
    loadChildren: () => import('./private/account-information/account/account.module').then( m => m.AccountPageModule),
    canActivate:[AuthGuard],
  },
  {
    path: 'allprojects',
    children:[
      {
        path:'',
        loadChildren: () => import('./public/bankofprojects/allprojects/allprojects.module').then( m => m.AllprojectsPageModule)
      },
      {
        path: 'projectsdetails/:id',
        loadChildren: () => import('./public/bankofprojects/projectsdetails/projectsdetails.module').then( m => m.ProjectsdetailsPageModule)
      }
    ]
  },
  {
    path: 'benefits',
    loadChildren: () => import('./public/benefits/benefits.module').then( m => m.BenefitsPageModule)
  },
  {
    path: 'admin-dashboard',
    children:[
      {
        path:'',
        loadChildren: () => import('./private/admin/admin-dashboard/admin-dashboard.module').then( m => m.AdminDashboardPageModule),
        canActivate:[AuthGuard],
        data:{
          role: 'Admin'
        }
      },
      {
        path: 'new-admin-modal',
        loadChildren: () => import('./private/admin/new-admin-modal/new-admin-modal.module').then( m => m.NewAdminModalPageModule),
        canActivate:[AuthGuard],
        data:{
          role: 'Admin'
        }
      },
      {
        path: 'edit-modal/:id',
        loadChildren: () => import('./private/admin/edit-modal/edit-modal.module').then( m => m.EditModalPageModule),
        canActivate:[AuthGuard],
        data:{
          role: 'Admin'
        }
      },
      {
        path: 'edit-modal-projects/:id',
        loadChildren: () => import('./private/admin/edit-modal-projects/edit-modal-projects.module').then( m => m.EditModalProjectsPageModule),
        canActivate:[AuthGuard],
        data:{
          role: 'Admin'
        }
      }
    ]
  },
  {
    path: 'video-popup',
    loadChildren: () => import('./public/video-popup/video-popup.module').then( m => m.VideoPopupPageModule)
  },
  {
    path: 'carbonfootprintform',
    loadChildren: () => import('./public/carbonfootprintform/carbonfootprintform.module').then( m => m.CarbonfootprintformPageModule)
  },
  {
    path: 'intro',
    loadChildren: () => import('./public/intro/intro.module').then( m => m.IntroPageModule)
  },
  {
    path: 'plan-accion',
    loadChildren: () => import('./public/plan-accion/plan-accion.module').then( m => m.PlanAccionPageModule)
  },
  {
    path: 'tutorial',
    loadChildren: () => import('./public/tutorial/tutorial.module').then( m => m.TutorialPageModule),
    canLoad:[CheckTutorialGuard]
  },
  {
    path: 'patrocinadores',
    loadChildren: () => import('./public/patrocinadores/patrocinadores.module').then( m => m.PatrocinadoresPageModule)
  },  {
    path: 'estate-project',
    loadChildren: () => import('./private/admin/estate-project/estate-project.module').then( m => m.EstateProjectPageModule)
  }


];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}


