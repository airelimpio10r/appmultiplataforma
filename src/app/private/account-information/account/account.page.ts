import { Component, OnInit, ChangeDetectorRef, ElementRef, ViewChild } from '@angular/core';
import { AuthService } from '../../../core/service/auth.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { FirestoreCRUDService } from 'src/app/core/service/firestore/firestore-crud.service';
import { LogicReuseService } from 'src/app/shared/resources/logic-reuse.service';
import { User } from 'src/app/shared/user.interface';

@Component({
  selector: 'app-account',
  templateUrl: './account.page.html',
  styleUrls: ['./account.page.scss'],
})
export class AccountPage implements OnInit {

  selectTabs = 'EditarPerfil';
  dataUser = this.CRUD.dataUser;
  // Data Form
  UpdatePNatural: FormGroup;
  RegistryBusiness: FormGroup;
  // objetos vacios para llenar con los datos del select
  Departmentchoose = [];
  Municipalitychoose = [];
  countries;
  options = ['Cédula de Ciudadanía', 'Cédula de Extranjería', 'Carné diplomatico', 'Pasaporte', 'Permiso especial de permanencia',];
  Actividad = ['Agricultura y ganadería', 'Bienes de consumo', 'Comercio electrónico', 'Construcción', 'Alimentación', 'Deporte y ocio',
    'Energía', 'Finanzas, seguros y bienes inmuebles', 'Lógistica y transporte', 'Medios de comunicación y marketing', 'Metalurgia y electrónica', 'Productos químicos y materias primas',
    'Salud e industria farmacéutica', 'Educación', 'Tecnología y telecomunicaciones', 'Turismo y hotelería', 'Fabricación y manufacturación',
  ];
  // Mesajes de error en las entradas de datos de los forms
  validation_messages;
  //Boleano que sirve para activar los alert de los input untouched
  inputUntouched: boolean = false;

  constructor(
    private formBuilder: FormBuilder, private _cdr: ChangeDetectorRef,
    private CRUD: FirestoreCRUDService, private logicReuse: LogicReuseService,
    private authServise: AuthService
  ) { }

  ngOnInit() {
    this.UpdatePNatural = this.FormPNatural();
    this.RegistryBusiness = this.FormBusiness();
    this.countries = this.logicReuse.countries;
    this.validation_messages = this.logicReuse.validation_messages;
    this.showData();
  }

  //Mostrar datos
  showData() {
    var rol;
    this.dataUser.forEach(element => {
      this.RegistryBusiness.get('nit').setValue(element.nit);
      this.RegistryBusiness.get('companyName').setValue(element.companyName);
      this.UpdatePNatural.get('document').setValue(element.document);
      this.UpdatePNatural.get('name').setValue(element.name);
      this.UpdatePNatural.get('lastName').setValue(element.lastName);
      this.UpdatePNatural.get('cellPhone').setValue(element.cellPhone);
      this.UpdatePNatural.get('telephone').setValue(element.telephone);
      this.UpdatePNatural.get('direction').setValue(element.direction);
      rol = element.rol
    });
  }
  //Borra el usuario del servicio de autentificación 
  deleteUserCurrent() {
    this.authServise.deleteUser().catch(err => this.authServise.alertMessaje(err));
  }
  //Envia link de restauración de contraseña
  sendLinkReset() {
    this.authServise.resetPassword(this.authServise.emailUser).then(() => {
      this.logicReuse.presentToastSuccess(' <ion-icon name="checkmark-circle"></ion-icon> Se envió un correo electrónico con un enlace para cambiar su contraseña.');
    }).catch(err => this.authServise.alertMessaje(err));
  }
  //Busqueda y carga de imagen
  handleImage(event: any): void {
    const image = event.target.files[0];
    this.CRUD.updateImageProfile(image);
  }
  //Evalua el uso del botton enter para hacer el submit
  eventHandler(keyCode) {
    if (keyCode === 13) {
      if (this.UpdatePNatural.valid) {
        // Trigger the button element with a click
        document.getElementById("bttSubmit").click();
      }
    }
  }
  // Activa el evento ionChange para introducir datos a los selects
  oncountryChange() {
    let country = this.UpdatePNatural.get('country').value; //Pide el valor del Select pais
    this.Departmentchoose = this.logicReuse.CountryDepartment[country]; // introduce en el Select las opciones de acuerdo a lo escogido
    document.getElementById('department').style.display = "block";
    this._cdr.detectChanges();
  }
  departmentChange() {
    let department = this.UpdatePNatural.get('department').value;//Pide el valor del Select departamento
    this.Municipalitychoose = this.logicReuse.MunicipalDepartment[department];// introduce en el Select las opciones de acuerdo a lo escogido
    document.getElementById('municipality').style.display = "block";
    this._cdr.detectChanges();
  }
  // Donde se guardan los datos del form
  saveDataPNatural(record) {
    try {
      //valida que este lleno el formulario
      if (this.UpdatePNatural.valid) {
        const data: User = {
          name: record.name,
          lastName: record.lastName,
          documentType: record.documentType,
          document: record.document,
          cellPhone: record.cellPhone,
          direction: record.direction,
          country: record.country,
          department: record.department,
          municipality: record.municipality,
          telephone: record.telephone,
        };
        this.CRUD.updateProfile(data);
        this.UpdatePNatural.reset();
        this.logicReuse.presentToastSuccess('<ion-icon name="checkmark-circle"></ion-icon>  La actualizacion de datos fue exitosa');
        this.inputUntouched = false;
      } else {
        this.inputUntouched = true;
      }
    } catch (error) {
      console.log(error)
    }
  }
  // Condiciones de la entrada de datos form
  private FormPNatural() {
    return this.formBuilder.group({
      nit: ['', [Validators.minLength(2)]],
      companyName: ['', [Validators.minLength(2)]],
      companyActivity: ['', [Validators.minLength(2)]],
      documentType: [''],
      document: ['', [Validators.pattern('[0-9]*')]],
      name: ['', [Validators.pattern('[a-zA-Z ]*'), Validators.minLength(2)]],
      lastName: ['', [Validators.pattern('[a-zA-Z ]*'), Validators.minLength(2)]],
      country: [''],
      department: [''],
      municipality: [''],
      cellPhone: ['', [Validators.minLength(10), Validators.maxLength(10), Validators.pattern('[0-9]*')]],
      telephone: ['', [Validators.minLength(7), Validators.maxLength(7), Validators.pattern('[0-9]*')]],
      direction: ['', [Validators.minLength(2)]]
    });
  }
  // Donde se guardan los datos del form
  saveDataBusiness(record) {
    try {
      //valida que este lleno el formulario
      if (this.RegistryBusiness.valid) {
        const data: User = {
          nit: record.nit,
          companyName: record.companyName,
          companyActivity: record.companyActivity
        };
        this.CRUD.updateProfile(data);
        this.RegistryBusiness.reset();
        this.logicReuse.presentToastSuccess('<ion-icon name="checkmark-circle"></ion-icon>  La actualizacion de datos fue exitosa');
        this.inputUntouched = false;
      } else {
        this.inputUntouched = true;
      }
    } catch (error) {
      console.log(error)
    }
  }
  // Condiciones de la entrada de datos form
  private FormBusiness() {
    return this.formBuilder.group({
      nit: ['', [Validators.minLength(2)]],
      companyName: ['', [Validators.minLength(2)]],
      companyActivity: ['', [Validators.minLength(2)]],
    });
  }
}