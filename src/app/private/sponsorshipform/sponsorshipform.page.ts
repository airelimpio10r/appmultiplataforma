import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { FirestoreCRUDService } from 'src/app/core/service/firestore/firestore-crud.service';
import { LogicReuseService } from 'src/app/shared/resources/logic-reuse.service';
import { Router } from '@angular/router';
import { Sponsor } from 'src/app/shared/patrocinio.interface';
import { AuthService } from 'src/app/core/service/auth.service';
import { ModalImagePage } from 'src/app/public/bankofprojects/projectsdetails/modal-image/modal-image.page';
import { ModalController } from '@ionic/angular';

@Component({
  selector: 'app-sponsorshipform',
  templateUrl: './sponsorshipform.page.html',
  styleUrls: ['./sponsorshipform.page.scss'],
})
export class SponsorshipformPage implements OnInit {

  dataSponsorship: FormGroup;
  monthlyPrice: number;
  annualPrice: number;
  valueCarbonFoot: number;
  Rol: string;
  Tree: number;
  date: Date = new Date();
  active: boolean = false;
  validation_messages;

  constructor(
    private logicReuse: LogicReuseService, private CRUD: FirestoreCRUDService,
    private formBuilder: FormBuilder, private authService: AuthService,
    private router: Router, private modalCtrl: ModalController
  ) {
  }

  ngOnInit() {
    this.validation_messages = this.logicReuse.validation_messages;
    this.dataSponsorship = this.FromAspLegales();
    this.priceCalculation();
    this.hiddenHTML();
  }
  //Esconde partes del html cuando esta logiado
  hiddenHTML() {
    this.CRUD.dataUser.forEach(element => {
      this.Rol = element.rol
    });
    this.authService.user$.subscribe(data => {
      if (data) {
        this.active = true;
      } else {
        this.active = false;
      }
    })
  }
  //Evalua el uso del botton enter para hacer el submit
  eventHandler(keyCode) {
    if (keyCode === 13) {
      if (this.dataSponsorship.valid) {
        // Trigger the button element with a click
        document.getElementById("buttonNext").click();
      }
    }
  }
  //calculo de la huella de carbono   
  priceCalculation() {
    if (this.Rol == 'Empresa') {
      this.valueCarbonFoot = this.dataSponsorship.value.carbonfootprint;
      this.Tree = Math.round(2.5001 * (this.valueCarbonFoot) - 0.0445);
      this.annualPrice = this.valueCarbonFoot * 50000;
      var price = this.annualPrice / 12;
      this.monthlyPrice = Math.trunc(price);
      // sirve para agregar un valor a un input y que este se pueda modificar
      this.dataSponsorship.get('amountMoney').setValue(this.monthlyPrice);
    } else {
      this.valueCarbonFoot = this.logicReuse.carbonFootprintCalculation;
      this.Tree = Math.round(2.5001 * (this.valueCarbonFoot) - 0.0445);
      this.dataSponsorship.value.carbonfootprint = this.valueCarbonFoot;
      this.annualPrice = Math.trunc(this.valueCarbonFoot * 50000);
      var price = this.annualPrice / 12;
      // if (isNaN(this.annualPrice) || this.logicReuse.carbonFootprintCalculation == 0 ) {
      //   this.router.navigate(['/carbonfootprintform']);
      // } else {
      this.monthlyPrice = Math.trunc(price);
      // sirve para agregar un valor a un input y que este se pueda modificar
      this.dataSponsorship.get('amountMoney').setValue(this.monthlyPrice);
      // }
    }
  }
  //Envia los datos al servicio
  loadService(value) {
    var randomId = Math.random().toString(36).substring(2, 8);
    this.priceCalculation();
    if (this.active == true) {
      const data: Sponsor = {
        id: randomId,
        carbonfootprint: value.carbonfootprint,
        carbonPorcentages: this.logicReuse.carbonFootprintPorcentages,
        amountMoney: this.monthlyPrice,
        annualPrice: this.annualPrice,
        contributionFrequency: value.contributionFrequency,
        user: {
          email: this.authService.emailUser
        },
        date: this.date,
      }
      this.CRUD.createSponsorship(data);
      this.dataSponsorship.reset(); // borramos los datos del form
      this.popUp();
      this.router.navigate(['/home']);
      // this.logicReuse.carbonFootprintCalculation = 0;
    } else if (this.dataSponsorship.value.name == '' || this.dataSponsorship.value.lastname == '' || this.dataSponsorship.value.cellPhone == '' || this.dataSponsorship.value.email == '') {
      this.logicReuse.presentToast(' <ion-icon name="alert-circle-outline"></ion-icon> El formulario no esta diligenciado correctamente.');
    } else {
      const data: Sponsor = {
        id: randomId,
        carbonfootprint: this.valueCarbonFoot,
        carbonPorcentages: this.logicReuse.carbonFootprintPorcentages,
        user: {
          email: value.email
        },
        amountMoney: this.monthlyPrice,
        annualPrice: this.annualPrice,
        date: this.date,
        name: value.name,
        lastName: value.lastName,
        cellPhone: value.cellPhone,
        contributionFrequency: value.contributionFrequency,
      }
      this.CRUD.createSponsorship(data);
      this.dataSponsorship.reset(); // borramos los datos del form 
      this.popUp();
      this.dataSponsorship = this.FromAspLegales();
      this.router.navigate(['/home']);
      // this.logicReuse.carbonFootprintCalculation = 0;
    }
  }
  // Donde se guardan los datos del form
  saveData(value) {
    // Valida si el formulario esta completo (otra forma de validad un reactive from)
    if (this.Rol == 'Empresa') {
      if (!this.dataSponsorship.value.carbonfootprint) {
        this.logicReuse.presentToast(' <ion-icon name="alert-circle-outline"></ion-icon> El formulario no esta diligenciado correctamente.');
        return;
      } else {
        this.loadService(value)
      }
    } else {
      if (!this.logicReuse.carbonFootprintCalculation) {
        this.logicReuse.presentToast(' <ion-icon name="alert-circle-outline"></ion-icon> El formulario no esta diligenciado correctamente.');
        return;
      } else {
        this.loadService(value);
      }
    }
  }
  // Condiciones de la entrada de datos form
  private FromAspLegales() {
    return this.formBuilder.group({
      name: ['', [Validators.required, Validators.pattern('[a-zA-Z ]*'), Validators.minLength(2)]],
      lastName: ['', [Validators.required, Validators.pattern('[a-zA-Z ]*'), Validators.minLength(2)]],
      cellPhone: ['', [Validators.required, Validators.minLength(10), Validators.maxLength(10), Validators.pattern('[0-9]*')]],
      email: ['', [Validators.required, Validators.email]],
      carbonfootprint: ['0', [Validators.required, Validators.pattern('[0-9]*')]],
      amountMoney: ['0', [Validators.required, Validators.pattern('[0-9]*')]],
      contributionFrequency: ['Mensual'],
    });
  }
  // Carga una imagen en una page como popup
  async popUp() {
    const modal = this.modalCtrl.create({
      component: ModalImagePage,
      cssClass: 'my-modal2-css',
    });
    return (await modal).present()
  }
}
