import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { SponsorshipformPage } from './sponsorshipform.page';

describe('SponsorshipformPage', () => {
  let component: SponsorshipformPage;
  let fixture: ComponentFixture<SponsorshipformPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SponsorshipformPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(SponsorshipformPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
