import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ModalController } from '@ionic/angular';
import { AdminDashboardService } from 'src/app/core/service/dashboard/admin-dashboard.service';
import { FirestoreCRUDService } from 'src/app/core/service/firestore/firestore-crud.service';
import { LogicReuseService } from 'src/app/shared/resources/logic-reuse.service';
import { User } from 'src/app/shared/user.interface';

@Component({
  selector: 'app-edit-modal',
  templateUrl: './edit-modal.page.html',
  styleUrls: ['./edit-modal.page.scss'],
})
export class EditModalPage implements OnInit {

  detallesUSer = this.dashboard.detallesUser
  // Data Form
  UpdatePNatural: FormGroup;
  // objetos vacios para llenar con los datos del select
  Departmentchoose = [];
  Municipalitychoose = [];
  countries;
  options = ['Cédula de Ciudadanía', 'Cédula de Extranjería', 'Carné diplomatico', 'Pasaporte', 'Permiso especial de permanencia',];
  Actividad = ['Agricultura y ganadería', 'Bienes de consumo', 'Comercio electrónico', 'Construcción', 'Alimentación', 'Deporte y ocio',
    'Energía', 'Finanzas, seguros y bienes inmuebles', 'Lógistica y transporte', 'Medios de comunicación y marketing', 'Metalurgia y electrónica', 'Productos químicos y materias primas',
    'Salud e industria farmacéutica', 'Educación', 'Tecnología y telecomunicaciones', 'Turismo y hotelería', 'Fabricación y manufacturación',
  ];
  // Mesajes de error en las entradas de datos de los forms
  validation_messages;
  //Boleano que sirve para activar los alert de los input untouched
  inputUntouched: boolean = false;
  //booleanos 
  activateCompanySection: boolean = false;
  activateEdit: boolean = true;
  emailUser: string;

  constructor(
    private modalCtrl: ModalController, private router: Router,
    private dashboard: AdminDashboardService,
    private formBuilder: FormBuilder, private _cdr: ChangeDetectorRef,
    private CRUD: FirestoreCRUDService, private logicReuse: LogicReuseService,
  ) { }

  ngOnInit() {
    this.UpdatePNatural = this.FormPNatural();
    this.countries = this.logicReuse.countries;
    this.validation_messages = this.logicReuse.validation_messages;
    this.showData()
  }

  //Mostrar datos
  showData() {
    this.detallesUSer.forEach(element => {
      if(element.rol == 'Empresa'){
        this.UpdatePNatural.get('nit').setValue(element.nit);
        this.UpdatePNatural.get('companyName').setValue(element.companyName);
        this.UpdatePNatural.get('companyActivity').setValue(element.companyActivity);
        this.activateCompanySection == true;
      }
      this.UpdatePNatural.get('documentType').setValue(element.documentType);
      this.UpdatePNatural.get('document').setValue(element.document);
      this.UpdatePNatural.get('name').setValue(element.name);
      this.UpdatePNatural.get('lastName').setValue(element.lastName);
      this.UpdatePNatural.get('cellPhone').setValue(element.cellPhone);
      this.UpdatePNatural.get('telephone').setValue(element.telephone);
      this.UpdatePNatural.get('direction').setValue(element.direction);
      this.emailUser = element.email
    });
  }

  //Evalua el uso del botton enter para hacer el submit
  eventHandler(keyCode) {
    if (keyCode === 13) {
      if (this.UpdatePNatural.valid) {
        // Trigger the button element with a click
        document.getElementById("bttSubmit").click();
      }
    }
  }
  // Activa el evento ionChange para introducir datos a los selects
  oncountryChange() {
    let country = this.UpdatePNatural.get('country').value; //Pide el valor del Select pais
    this.Departmentchoose = this.logicReuse.CountryDepartment[country]; // introduce en el Select las opciones de acuerdo a lo escogido
    this._cdr.detectChanges();
  }
  departmentChange() {
    let department = this.UpdatePNatural.get('department').value;//Pide el valor del Select departamento
    this.Municipalitychoose = this.logicReuse.MunicipalDepartment[department];// introduce en el Select las opciones de acuerdo a lo escogido
    this._cdr.detectChanges();
  }
  // Donde se guardan los datos del form
  saveData(record) {
    //valida que este lleno el formulario
    if (this.UpdatePNatural.valid) {
      const data: User = {
        nit: record.nit,
        companyName: record.companyName,
        companyActivity: record.companyActivity,
        name: record.name,
        lastName: record.lastName,
        documentType: record.documentType,
        document: record.document,
        cellPhone: record.cellPhone,
        direction: record.direction,
        country: record.country,
        department: record.department,
        municipality: record.municipality,
        telephone: record.telephone,
      };
      this.dashboard.updateProfile(data, this.emailUser);
      this.UpdatePNatural.reset();
      this.logicReuse.presentToastSuccess('<ion-icon name="checkmark-circle"></ion-icon>  La actualizacion de datos fue exitosa');
      this.router.navigate(['/admin-dashboard']);
      this.inputUntouched = false;
    } else {
      this.inputUntouched = true;
    }
  }
  // Condiciones de la entrada de datos form
  private FormPNatural() {
    return this.formBuilder.group({
      nit: ['', [Validators.minLength(2)]],
      companyName: ['', [Validators.minLength(2)]],
      companyActivity: ['', [Validators.minLength(2)]],
      documentType: ['', Validators.required],
      document: ['', [Validators.pattern('[0-9]*'), Validators.required]],
      name: ['', [Validators.required, Validators.pattern('[a-zA-Z ]*'), Validators.minLength(2)]],
      lastName: ['', [Validators.required, Validators.pattern('[a-zA-Z ]*'), Validators.minLength(2)]],
      country: ['', Validators.required],
      department: ['', Validators.required],
      municipality: ['', Validators.required],
      cellPhone: ['', [Validators.required, Validators.minLength(10), Validators.maxLength(10), Validators.pattern('[0-9]*')]],
      telephone: ['', [Validators.minLength(7), Validators.maxLength(7), Validators.pattern('[0-9]*')]],
      direction: ['', [Validators.required, Validators.minLength(2)]]
    });
  }

  cerrarModal() {
    this.router.navigate(['/admin-dashboard'])
  }
}
