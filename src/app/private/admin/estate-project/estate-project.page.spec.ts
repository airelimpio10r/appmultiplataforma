import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { EstateProjectPage } from './estate-project.page';

describe('EstateProjectPage', () => {
  let component: EstateProjectPage;
  let fixture: ComponentFixture<EstateProjectPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EstateProjectPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(EstateProjectPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
