import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ModalController } from '@ionic/angular';
import { AdminDashboardService } from 'src/app/core/service/dashboard/admin-dashboard.service';
import { RegisterProject } from 'src/app/shared/registrarproyecto.interface';

@Component({
  selector: 'app-estate-project',
  templateUrl: './estate-project.page.html',
  styleUrls: ['./estate-project.page.scss'],
})
export class EstateProjectPage implements OnInit {

  detailsDoc = this.dashboard.detallesProject;
  inputUntouched: boolean = false;
  FormEvaluacion: FormGroup;
  validation_messages1 = {
    'validate': [
      { type: 'required', message: 'El estado del proyecto es requerido.' },
    ],
    'bodymessage': [
      { type: 'required', message: 'Se requiere el cuerpo del mensaje.' },
      { type: 'maxlength', message: 'El máximo de caracteres es de 1600.' },
      { type: 'minlength', message: 'El mínimo de caracteres es de 2.' },
    ],
  }
  // activateEdit: boolean = true;
  
  constructor(
    private router: Router, private formBuilder: FormBuilder,
    private dashboard: AdminDashboardService,private modalCtrl: ModalController
  ) { }
  
  cerrarModal() {
    // this.router.navigate(['/admin-dashboard'])
    this.modalCtrl.dismiss();
  }
  ngOnInit() {
    if (this.detailsDoc.length == 0) {
      this.modalCtrl.dismiss();
    }
    this.FormEvaluacion = this.FormValid()
  }
  
  // Carga de la aprobacion o declinacion del proyecto
  projectValidation(value, projectName) {
    if (this.FormEvaluacion.valid) {
      const data: RegisterProject = {
        projectName: projectName,
        Validate: value.validate,
        RTAValidacion: value.bodymessage,
      }
      this.dashboard.updateProjects(data);
     
      this.FormEvaluacion.reset();
      this.inputUntouched = false;
      this.modalCtrl.dismiss();
    } else {
      this.inputUntouched = true;
    }
  }
  //Evalua el uso del botton enter para hacer el submit
  eventHandler(keyCode) {
    if (keyCode === 13) {
      if (this.FormEvaluacion.valid) {
        // Trigger the button element with a click
        document.getElementById("bttSubmitContactanos").click();
      }
    }
  }

  // Condiciones de la entrada de datos form
  private FormValid() {
    return this.formBuilder.group({
      validate: ['', [Validators.required]],
      bodymessage: ['', [Validators.required, Validators.maxLength(1600), Validators.minLength(2)]]
    })
  }
}
