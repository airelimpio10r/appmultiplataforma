import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { EstateProjectPage } from './estate-project.page';

const routes: Routes = [
  {
    path: '',
    component: EstateProjectPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class EstateProjectPageRoutingModule {}
