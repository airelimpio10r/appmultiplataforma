import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { EstateProjectPageRoutingModule } from './estate-project-routing.module';

import { EstateProjectPage } from './estate-project.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ReactiveFormsModule,
    EstateProjectPageRoutingModule
  ],
  declarations: [EstateProjectPage]
})
export class EstateProjectPageModule {}
