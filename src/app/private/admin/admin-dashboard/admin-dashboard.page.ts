import { Component, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { ModalController } from '@ionic/angular';
import { element } from 'protractor';
import { AuthService } from 'src/app/core/service/auth.service';
import { AdminDashboardService } from 'src/app/core/service/dashboard/admin-dashboard.service';
import { FirestoreCRUDService } from 'src/app/core/service/firestore/firestore-crud.service';
import { Sponsor } from 'src/app/shared/patrocinio.interface';
import { Pqr } from 'src/app/shared/pqr.interface';
import { RegisterProject } from 'src/app/shared/registrarproyecto.interface';
import { LogicReuseService } from 'src/app/shared/resources/logic-reuse.service';
import { User } from 'src/app/shared/user.interface';
import { EstateProjectPage } from '../estate-project/estate-project.page';
import { NewAdminModalPage } from '../new-admin-modal/new-admin-modal.page';

@Component({
  selector: 'app-admin-dashboard',
  templateUrl: './admin-dashboard.page.html',
  styleUrls: ['./admin-dashboard.page.scss'],
})
export class AdminDashboardPage implements OnInit {

  //Arrays de los Search
  usuarios: User[] = [];
  PQRs: Pqr[] = [];
  dataUser = this.CRUD.dataUser;
  UpdatePNatural: FormGroup;
  RegistryBusiness: FormGroup;
  Proyectos: RegisterProject[] = [];
  Sponsor: Sponsor[] = [];
  selectTabs = 'Usuarios';
  //Datos de la base de datos
  collectionPqr: Pqr[] = [];
  collectionUsers: User[] = [];
  collectionSponsorShip: Sponsor[] = [];
  collectionProject: RegisterProject[] = [];

  constructor(
    private modalCtrl: ModalController, private authServise: AuthService, private CRUD: FirestoreCRUDService,
    private dashboard: AdminDashboardService, private router: Router, private logicReuse: LogicReuseService,
  ) { }

  ngOnInit() {
    this.loadDataPQR();
    this.loadDataUsers();
    this.loadDataSponsor();
    this.loadDataRegisterProject();
  }
  async Estate() {
    const modal2 = this.modalCtrl.create({
      component: EstateProjectPage,
      cssClass: 'my-modal-css',
    });
    return (await modal2).present()
  }

  prueba(data){
    this.dashboard.detallesProject.pop();
    this.dashboard.detallesProject.push(data);
    this.Estate();
  }

  // Permite buscar y clasificar proyectos
  searchProyect(event) {
    const texto = event.target.value;
    this.Proyectos = [];
    this.collectionProject.forEach(data => {
      if (data.user.email == texto || data.projectName == texto) {
        const document: RegisterProject = {
          projectType: data.projectType,
          projectName: data.projectName,
          forestArea: data.forestArea,
          forestType: data.forestType,
          temperature: data.temperature,
          typeTemperature: data.typeTemperature,
          treeDiameter: data.treeDiameter,
          treeAge: data.treeAge,
          treeSize: data.treeSize,
          country: data.country,
          department: data.department,
          municipality: data.municipality,
          zone: data.zone,
          msnm: data.msnm,
          propertyName: data.propertyName,
          cadastralCertificate: data.cadastralCertificate,
          enrollment: data.enrollment,
          treeSpecies: data.treeSpecies,
          photos: data.photos,
          date: data.date,
          Validate: data.Validate,
          user: {
            email: data.user.email
          },
          RTAValidacion: data.RTAValidacion
        }
        this.Proyectos.push(document);
      }
    })
  }
  // Permite buscar y clasificar pQRS
  searchPQR(event) {
    const texto = event.target.value;
    this.PQRs = [];
    this.collectionPqr.forEach(data => {
      if (data.email == texto || data.radicado == texto) {
        const document: Pqr = {
          radicado: data.radicado,
          email: data.email,
          pqr: data.pqr,
          date: data.date,
          estado: data.estado,
        }
        this.PQRs.push(document);
      }
    })
  }
  // Permite buscar y clasificar aportes
  searchSponsor(event) {
    const texto = event.target.value;
    this.Sponsor = [];
    this.collectionSponsorShip.forEach(data => {
      if (data.user.email == texto || data.id == texto) {
        const document: Sponsor = {
          id: data.id,
          carbonfootprint: data.carbonfootprint,
          amountMoney: data.amountMoney,
          annualPrice: data.annualPrice,
          contributionFrequency: data.contributionFrequency,
          user: {
            email: data.user.email,
          },
          name: data.name,
          lastName: data.lastName,
          cellPhone: data.cellPhone,
          carbonPorcentages: {
            CalculodeElectricidad: data.carbonPorcentages.CalculodeElectricidad,
            CalculodeAlimentacion: data.carbonPorcentages.CalculodeAlimentacion,
            CalculodeTransporteTerrestre: data.carbonPorcentages.CalculodeTransporteTerrestre,
            CalculodeTransporteAereo: data.carbonPorcentages.CalculodeTransporteAereo,
          }
        }
        this.Sponsor.push(document);
      }
    })
  }
  // Permite buscar y clasificar Usuarios
  searchUser(event) {
    const texto = event.target.value;
    this.usuarios = [];
    this.collectionUsers.forEach(data => {
      if (data.email == texto) {
        const document: User = {
          name: data.name,
          lastName: data.lastName,
          password: data.password,
          email: data.email,
          nit: data.nit,
          companyName: data.companyName,
          companyActivity: data.companyActivity,
          documentType: data.documentType,
          document: data.document,
          cellPhone: data.cellPhone,
          direction: data.direction,
          rol: data.rol,
          country: data.country,
          department: data.department,
          municipality: data.municipality,
          telephone: data.telephone,
          date: data.date
        }
        this.usuarios.push(document);
      }
    });
  }
  // Guarda y envia los datos del Proyecto a otra page
  detallesRegisterProject(data) {
    this.dashboard.detallesProject.pop();
    this.dashboard.detallesProject.push(data);
    this.router.navigate([`/admin-dashboard/edit-modal-projects/${data.projectName}`]);
  }
  // detallesEstateProject(data) {
  //   this.dashboard.detallesProject.pop();
  //   this.dashboard.detallesProject.push(data);
  //   this.router.navigate([`/admin-dashboard/estate-project/${data.projectName}`]);
  // }
  //Guarda y envia los datos del Sponsor a otra page
  detallesSponsor(data) {
    this.dashboard.detallesSponsor.pop();
    this.dashboard.detallesSponsor.push(data);
    this.popUp();
  }
  //Activar el refres
  doRefresh(event) {
    this.loadDataPQR();
    this.loadDataUsers();
    this.loadDataSponsor();
    this.loadDataRegisterProject();
    setTimeout(() => {
      event.target.complete();
    }, 2000);
  }
  // Envia un link para recuperacion de contraseña
  sendLinkReset(email) {
    this.authServise.resetPassword(email);
    this.logicReuse.presentToastSuccess(' <ion-icon name="checkmark-circle"></ion-icon> Se envió un correo electrónico con un enlace para restablecer su contraseña.');
  }
  // Guarda y envia los datos del usuario a otra page
  detallesUser(value) {
    this.dashboard.detallesUser.pop();
    this.dashboard.detallesUser.push(value);
    this.router.navigate([`/admin-dashboard/edit-modal/${value.name}`]);
  }
  //Carga la información del Proyectos de la base de datos
  loadDataRegisterProject() {
    this.CRUD.getAllDocument().subscribe(value => {
      this.collectionProject = [];
      value.docs.forEach(element1 => {
        const data = element1.data()
        const document: RegisterProject = {
          projectType: data.projectType,
          projectName: data.projectName,
          forestArea: data.forestArea,
          forestType: data.forestType,
          temperature: data.temperature,
          typeTemperature: data.typeTemperature,
          treeDiameter: data.treeDiameter,
          treeAge: data.treeAge,
          treeSize: data.treeSize,
          country: data.country,
          department: data.department,
          municipality: data.municipality,
          zone: data.zone,
          msnm: data.msnm,
          propertyName: data.propertyName,
          cadastralCertificate: data.cadastralCertificate,
          enrollment: data.enrollment,
          treeSpecies: data.treeSpecies,
          photos: data.photos,
          date: data.date,
          Validate: data.Validate,
          user: {
            email: data.user.email
          },
          RTAValidacion: data.RTAValidacion
        }
        this.collectionProject.push(document);
      })
    })
  }
  //Carga la información del usuario de la base de datos 
  loadDataSponsor() {
    this.dashboard.getAllSponsor().subscribe(value => {
      this.collectionSponsorShip = [];
      value.docs.forEach(element2 => {
        const data = element2.data();
        const document: Sponsor = {
          id: data.id,
          carbonfootprint: data.carbonfootprint,
          amountMoney: data.amountMoney,
          annualPrice: data.annualPrice,
          contributionFrequency: data.contributionFrequency,
          user: {
            email: data.user.email,
          },
          name: data.name,
          lastName: data.lastName,
          cellPhone: data.cellPhone,
          carbonPorcentages: {
            CalculodeElectricidad: data.carbonPorcentages.CalculodeElectricidad,
            CalculodeAlimentacion: data.carbonPorcentages.CalculodeAlimentacion,
            CalculodeTransporteTerrestre: data.carbonPorcentages.CalculodeTransporteTerrestre,
            CalculodeTransporteAereo: data.carbonPorcentages.CalculodeTransporteAereo,
          }
        }
        this.collectionSponsorShip.push(document);
      })
    })
  }
  //Carga la informacion del usuario de la base de datos
  loadDataUsers() {
    this.dashboard.getAllUsers().subscribe(value => {
      this.collectionUsers = [];
      value.docs.forEach(element3 => {
        const data = element3.data();
        const document: User = {
          name: data.name,
          lastName: data.lastName,
          password: data.password,
          email: data.email,
          nit: data.nit,
          companyName: data.companyName,
          companyActivity: data.companyActivity,
          documentType: data.documentType,
          document: data.document,
          cellPhone: data.cellPhone,
          direction: data.direction,
          rol: data.rol,
          country: data.country,
          department: data.department,
          municipality: data.municipality,
          telephone: data.telephone,
          date: data.date
        }
        this.collectionUsers.push(document);
      })
    })
  }
  // Carga los pqrs guardados de la base de datos
  loadDataPQR() {
    this.dashboard.getAllPQRSD().subscribe(value => {
      this.collectionPqr = [];
      value.docs.forEach(element4 => {
        const data = element4.data()
        const document: Pqr = {
          radicado: data.radicado,
          email: data.email,
          pqr: data.pqr,
          date: data.date,
          estado: data.estado,
        }
        this.collectionPqr.push(document);
      })
    })
  }

  async popUp() {
    const modal = this.modalCtrl.create({
      component: NewAdminModalPage,
      cssClass: 'my-modal-admin-css',

    });
    return (await modal).present()
  }
  //borra los datos en el Firebase
  delete(collection, record_id) {
    this.dashboard.deletedocument(collection, record_id);
    this.loadDataPQR();
    this.loadDataSponsor();
    this.loadDataRegisterProject();
  }

  /*evento para cambiar el toggle de pqr*/
  changeEstadoPQR(value) {
    if (value.estado == true) {
      const data: Pqr = {
        radicado: value.radicado,
        email: value.email,
        pqr: value.pqr,
        estado: true
      }
      this.dashboard.updatePQRSD(data);
    } else {
      const data: Pqr = {
        radicado: value.radicado,
        email: value.email,
        pqr: value.pqr,
        estado: false
      }
      this.dashboard.updatePQRSD(data);
    }
  }

}
