import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { NewAdminModalPage } from './new-admin-modal.page';

const routes: Routes = [
  {
    path: '',
    component: NewAdminModalPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class NewAdminModalPageRoutingModule {}
