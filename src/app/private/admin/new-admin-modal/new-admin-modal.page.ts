import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { AdminDashboardService } from 'src/app/core/service/dashboard/admin-dashboard.service';

@Component({
  selector: 'app-new-admin-modal',
  templateUrl: './new-admin-modal.page.html',
  styleUrls: ['./new-admin-modal.page.scss'],
})
export class NewAdminModalPage implements OnInit {

  detailsDoc = this.dashboard.detallesSponsor;

  constructor(
    private modalCtrl: ModalController,
    private dashboard: AdminDashboardService,
  ) { }

  ngOnInit() {
  }

  cerrarModal() {
    this.modalCtrl.dismiss();
  }
}
