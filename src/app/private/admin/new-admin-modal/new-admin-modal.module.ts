import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { NewAdminModalPageRoutingModule } from './new-admin-modal-routing.module';

import { NewAdminModalPage } from './new-admin-modal.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    NewAdminModalPageRoutingModule,
    ReactiveFormsModule
  ],
  declarations: [NewAdminModalPage]
})
export class NewAdminModalPageModule {}
