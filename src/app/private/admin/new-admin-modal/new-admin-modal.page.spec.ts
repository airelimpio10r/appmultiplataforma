import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { NewAdminModalPage } from './new-admin-modal.page';

describe('NewAdminModalPage', () => {
  let component: NewAdminModalPage;
  let fixture: ComponentFixture<NewAdminModalPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NewAdminModalPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(NewAdminModalPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
