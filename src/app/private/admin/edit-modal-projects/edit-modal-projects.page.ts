import { ChangeDetectorRef, Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { IonSlides, NavController } from '@ionic/angular';
import { AdminDashboardService } from 'src/app/core/service/dashboard/admin-dashboard.service';
import { FirestoreCRUDService } from 'src/app/core/service/firestore/firestore-crud.service';
import { LogicReuseService } from 'src/app/shared/resources/logic-reuse.service';

@Component({
  selector: 'app-edit-modal-projects',
  templateUrl: './edit-modal-projects.page.html',
  styleUrls: ['./edit-modal-projects.page.scss'],
})
export class EditModalProjectsPage implements OnInit {
  
  detailsDoc = this.dashboard.detallesProject;
  RegisterProject: FormGroup;
  // objetos vacios para llenar con los datos del select country department y municipality
  Departmentchoose = [];
  Municipalitychoose = [];
  countries;
  // Mesajes de error en las entradas de datos de los forms
  validation_messages = this.logicReuse.validation_messages
  // objetos vacios para llenar con los datos del select 
  typepfprojects = ['Conservación', 'Creación'];
  foresttype = ['Páramo', 'Bosque', 'Humedal'];l
  temperaturetype = ['°C', '°F'] //Fahrenheit  Celsius​
  Currentusage = ['Recreación', 'Turismo', 'Senderismo', 'Abandonado', 'Siembra'];
  zone = ['Rural', 'Urbano'];
  treespecies = ['Caucho Tequendama', 'Cedro', 'Fresno', 'Pino romerón', 'Roble', 'Yarumo', 'Ceiba', 'Guayacán Trébol', 'Encenillo',
    'Nogal', 'Palma de cera', 'Aliso', 'Carbonero', 'Árbol loco', 'Arrayan', 'Borrachero', 'Cajeto', 'Cedro de altura', 'Cedro rosado',
    'Ciro', 'Corono', 'Guayacán de Manizales', 'Laurel de cera', 'Mano de oso', 'Mortiño', 'Sauco', 'Siete cueros', 'Sauce',
    'Cámbulo', 'Caoba', 'Caracolí', 'Caucho sabanero', 'Ceiba bonga', 'Ceiba tolua', 'Gualanday', 'Matarratón', 'Nacedero', 'Nogal',
    'Ocobo', 'Samán', 'Guayacán Amarillo', 'Guayacán Rosado', 'Almendro'];
  treesize = ['1-5 mts', '5-10 mts', '10-20 mts', '20-30 mts', '30-50 mts', '50 mts o más'];
  weather = ['Frio', 'Templado', 'Seco', 'Tropical'];
  floortype = ['Arcilla', 'Limo', 'Grava'];
  ageoftree = ['0-5 años', '5-10 años', '10-20 años', '20-40 años', '40 años o más'];
  heightsealevel = ['1000-1500 mts', '1500-2500 mts', '2500-3500 mts', '3500 mts o más'];
  treediameter = ['10-15 cm', '15-25 cm', '25-35 cm', '35-50 cm', '1-2 mts', '2-4 mts', '4 mts o más'];
  
  activateEdit: boolean = true;
  imagePreview: any = [];
  imageCharge: any = [];
  //Boleano que sirve para activar los alert de los input untouched
  inputUntouched: boolean = false;

  constructor(
    private CRUD: FirestoreCRUDService, private navCtrl: NavController, private router: Router,
    private formBuilder: FormBuilder, private _cdr: ChangeDetectorRef,
    private logicReuse: LogicReuseService, private dashboard: AdminDashboardService,
  ) { }

  ngOnInit() {
    this.RegisterProject = this.FromRegisterProject();
    this.countries = this.logicReuse.countries;
    this.validation_messages = this.logicReuse.validation_messages;
    this.showData();
  }

  //Mostrar datos
  showData() {
    this.detailsDoc.forEach(element => {
      this.RegisterProject.get('projectName').setValue(element.projectName);
      this.RegisterProject.get('forestArea').setValue(element.forestArea);
      this.RegisterProject.get('cadastralCertificate').setValue(element.cadastralCertificate);
      this.RegisterProject.get('enrollment').setValue(element.enrollment);
      this.RegisterProject.get('propertyName').setValue(element.propertyName);
    });
  }
  cerrarModal() {
    this.router.navigate(['/admin-dashboard'])
  }
  //Evalua el uso del botton enter para hacer el submit
  eventHandler(keyCode) {
    if (keyCode === 13) {
      if (this.RegisterProject.valid) {
        // Trigger the button element with a click
        document.getElementById("send").click();
      }
    }
  }
  // Donde se guardan los datos del form
  saveData(data) {
    //valida que este lleno el formulario
    if (this.RegisterProject.valid) {
      this.dashboard.updateProjects(data)
      // this.RegisterProject.reset(); // borramos los datos del form
      this.logicReuse.presentToastSuccess('<ion-icon name="checkmark-circle"></ion-icon>  La actualización fue exitosa');
      this.router.navigate(['/admin-dashboard']);
      this.inputUntouched = false;
    } else {
      this.inputUntouched = true;
    }
  }
  // Condiciones de la entrada de datos form
  private FromRegisterProject() {
    return this.formBuilder.group({
      projectType: ['', Validators.required],
      projectName: ['', Validators.required],
      forestArea: ['', [Validators.required, Validators.pattern('[0-9].*'), Validators.min(1)]],
      forestType: ['', Validators.required],
      temperature: ['', [Validators.required, Validators.pattern('[0-9]*')]],
      typeTemperature: ['', Validators.required],
      treeDiameter: ['', Validators.required],
      treeAge: ['', Validators.required],
      treeSize: ['', Validators.required],
      country: ['', Validators.required],
      department: ['', Validators.required],
      municipality: ['', Validators.required],
      zone: ['', Validators.required],
      msnm: ['', [Validators.required]],
      propertyName: ['', Validators.required],
      cadastralCertificate: ['', Validators.required],
      enrollment: ['', Validators.required],
      treeSpecies: ['', Validators.required],
    });
  }
  // Activa el evento ionChange para introducir datos a los selects
  oncountryChange() {
    let country = this.RegisterProject.get('country').value; //Pide el valor del Select pais
    this.Departmentchoose = this.logicReuse.CountryDepartment[country]; // introduce en el Select las opciones de acuerdo a lo escogido
    document.getElementById('department').style.display = "block";
    this._cdr.detectChanges();
  }
  departmentChange() {
    let department = this.RegisterProject.get('department').value;//Pide el valor del Select departamento
    this.Municipalitychoose = this.logicReuse.MunicipalDepartment[department];// introduce en el Select las opciones de acuerdo a lo escogido
    document.getElementById('municipality').style.display = "block";
    this._cdr.detectChanges();
  }
}






// Codigo del formulario de validacion l
// detailsDoc = this.dashboard.detallesProject;
// inputUntouched: boolean = false;
// FormEvaluacion: FormGroup;
// validation_messages = {
//   'validate': [
//     { type: 'required', message: 'El estado del proyecto es requerido.' },
//   ],
//   'bodymessage': [
//     { type: 'required', message: 'Se requiere el cuerpo del mensaje.' },
//     { type: 'maxlength', message: 'El máximo de caracteres es de 1600.' },
//     { type: 'minlength', message: 'El mínimo de caracteres es de 2.' },
//   ],
// }
// activateEdit: boolean = true;

// constructor(
//   private router: Router, private formBuilder: FormBuilder,
//   private dashboard: AdminDashboardService,
// ) { }

// cerrarModal() {
//   this.router.navigate(['/admin-dashboard'])
// }
// ngOnInit() {
//   if (this.detailsDoc.length == 0) {
//     this.router.navigate(['/admin-dashboard'])
//   }
//   this.FormEvaluacion = this.FormValid()
// }

// // Carga de la aprobacion o declinacion del proyecto
// projectValidation(value, projectName) {
//   if (this.FormEvaluacion.valid) {
//     const data: RegisterProject = {
//       projectName: projectName,
//       Validate: value.validate,
//       RTAValidacion: value.bodymessage,
//     }
//     this.dashboard.updateProjects(data);
//     this.FormEvaluacion.reset();
//     this.inputUntouched = false;
//     this.router.navigate(['/admin-dashboard'])
//   } else {
//     this.inputUntouched = true;
//   }
// }
// //Evalua el uso del botton enter para hacer el submit
// eventHandler(keyCode) {
//   if (keyCode === 13) {
//     if (this.FormEvaluacion.valid) {
//       // Trigger the button element with a click
//       document.getElementById("bttSubmitContactanos").click();
//     }
//   }
// }
// // Condiciones de la entrada de datos form
// private FormValid() {
//   return this.formBuilder.group({
//     validate: ['', [Validators.required]],
//     bodymessage: ['', [Validators.required, Validators.maxLength(1600), Validators.minLength(2)]]
//   })
// }