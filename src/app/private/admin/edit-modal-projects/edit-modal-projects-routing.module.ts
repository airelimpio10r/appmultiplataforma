import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { EditModalProjectsPage } from './edit-modal-projects.page';

const routes: Routes = [
  {
    path: '',
    component: EditModalProjectsPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class EditModalProjectsPageRoutingModule {}
