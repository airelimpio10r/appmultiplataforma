import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { EditModalProjectsPageRoutingModule } from './edit-modal-projects-routing.module';

import { EditModalProjectsPage } from './edit-modal-projects.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    EditModalProjectsPageRoutingModule,
    ReactiveFormsModule
  ],
  declarations: [EditModalProjectsPage]
})
export class EditModalProjectsPageModule {}
