import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { EditModalProjectsPage } from './edit-modal-projects.page';

describe('EditModalProjectsPage', () => {
  let component: EditModalProjectsPage;
  let fixture: ComponentFixture<EditModalProjectsPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditModalProjectsPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(EditModalProjectsPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
