import { Component, OnInit, ChangeDetectorRef, ViewChild } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { IonSlides, NavController, ToastController } from '@ionic/angular';
import { Router } from '@angular/router';
import { LogicReuseService } from 'src/app/shared/resources/logic-reuse.service';
import { FirestoreCRUDService } from 'src/app/core/service/firestore/firestore-crud.service';

@Component({
  selector: 'app-projectregistrationform',
  templateUrl: './projectregistrationform.page.html',
  styleUrls: ['./projectregistrationform.page.scss'],
})
export class ProjectregistrationformPage implements OnInit {

  RegisterProject: FormGroup;
  // objetos vacios para llenar con los datos del select country department y municipality
  Departmentchoose = [];
  Municipalitychoose = [];
  countries;
  // Mesajes de error en las entradas de datos de los forms
  validation_messages = this.logicReuse.validation_messages
  // objetos vacios para llenar con los datos del select 
  typepfprojects = ['Conservación', 'Creación'];
  foresttype = ['Páramo', 'Bosque', 'Humedal'];
  temperaturetype = ['°C', '°F'] //Fahrenheit  Celsius​
  Currentusage = ['Recreación', 'Turismo', 'Senderismo', 'Abandonado', 'Siembra'];
  zone = ['Rural', 'Urbano'];
  treespecies = ['Caucho Tequendama', 'Cedro', 'Fresno', 'Pino romerón', 'Roble', 'Yarumo', 'Ceiba', 'Guayacán Trébol', 'Encenillo',
    'Nogal', 'Palma de cera', 'Aliso', 'Carbonero', 'Árbol loco', 'Arrayan', 'Borrachero', 'Cajeto', 'Cedro de altura', 'Cedro rosado',
    'Ciro', 'Corono', 'Guayacán de Manizales', 'Laurel de cera', 'Mano de oso', 'Mortiño', 'Sauco', 'Siete cueros', 'Sauce',
    'Cámbulo', 'Caoba', 'Caracolí', 'Caucho sabanero', 'Ceiba bonga', 'Ceiba tolua', 'Gualanday', 'Matarratón', 'Nacedero', 'Nogal',
    'Ocobo', 'Samán', 'Guayacán Amarillo', 'Guayacán Rosado', 'Almendro'];
  treesize = ['1-5 mts', '5-10 mts', '10-20 mts', '20-30 mts', '30-50 mts', '50 mts o más'];
  weather = ['Frio', 'Templado', 'Seco', 'Tropical'];
  floortype = ['Arcilla', 'Limo', 'Grava'];
  ageoftree = ['0-5 años', '5-10 años', '10-20 años', '20-40 años', '40 años o más'];
  heightsealevel = ['1000-1500 mts', '1500-2500 mts', '2500-3500 mts', '3500 mts o más'];
  treediameter = ['10-15 cm', '15-25 cm', '25-35 cm', '35-50 cm', '1-2 mts', '2-4 mts', '4 mts o más'];
  
  imagePreview: any = [];
  imageCharge: any = [];
  //Boleano que sirve para activar los alert de los input untouched
  inputUntouched: boolean = false;


  @ViewChild('slideWithNav', { static: false }) slideWithNav: IonSlides;
  @ViewChild('slideWithNav2', { static: false }) slideWithNav2: IonSlides;
  @ViewChild('slideWithNav3', { static: false }) slideWithNav3: IonSlides;

  sliderOne: any;
  sliderTwo: any;
  sliderThree: any;


  //Configuration for each Slider
  slideOptsOne = {
    initialSlide: 0,
    slidesPerView: 2,
    autoplay: true
  };
  slideOptsTwo = {
    initialSlide: 1,
    slidesPerView: 2,
    loop: true,
    centeredSlides: true,
    spaceBetween: 5,
  };
  slideOptsThree = {
    initialSlide: 0,
    slidesPerView: 2
  };
  constructor(
    private CRUD: FirestoreCRUDService, private navCtrl: NavController, private router: Router,
    private formBuilder: FormBuilder, private _cdr: ChangeDetectorRef,
    private logicReuse: LogicReuseService,
  ) {
    this.sliderOne =
    {
      isBeginningSlide: true,
      isEndSlide: false,

    };
    //Item object 
    this.sliderTwo =
    {
      isBeginningSlide: true,
      isEndSlide: false,
    };
    //Item object 
    this.sliderThree =
    {
      isBeginningSlide: true,
      isEndSlide: false,
    };
  }


  //Move to Next slide
  slideNext(object, slideView) {
    slideView.slideNext(500).then(() => {
      this.checkIfNavDisabled(object, slideView);
    });
  }

  //Move to previous slide
  slidePrev(object, slideView) {
    slideView.slidePrev(500).then(() => {
      this.checkIfNavDisabled(object, slideView);
    });;
  }

  //Method called when slide is changed by drag or navigation
  SlideDidChange(object, slideView) {
    this.checkIfNavDisabled(object, slideView);
  }

  //Call methods to check if slide is first or last to enable disbale navigation  
  checkIfNavDisabled(object, slideView) {
    this.checkisBeginning(object, slideView);
    this.checkisEnd(object, slideView);
  }

  checkisBeginning(object, slideView) {
    slideView.isBeginning().then((istrue) => {
      object.isBeginningSlide = istrue;
    });
  }
  checkisEnd(object, slideView) {
    slideView.isEnd().then((istrue) => {
      object.isEndSlide = istrue;
    });
  }
  ngOnInit() {
    this.RegisterProject = this.FromRegisterProject();
    this.countries = this.logicReuse.countries;
    this.validation_messages = this.logicReuse.validation_messages;
  }

  //Borra una imagen 
  deleteImagen() {
    this.imagePreview.pop();
  }
  //Busqueda y carga de imagen
  handleImage(event: any): void {
    this.imageCharge.push(event.target.files[0]);
    if (event.target.files && event.target.files[0]) {
      var reader = new FileReader();
      reader.readAsDataURL(event.target.files[0]); // read file as data url
      reader.onload = (event) => { // called once readAsDataURL is completed
        this.imagePreview.push(event.target["result"]);
      }
    }
  }
  //Evalua el uso del botton enter para hacer el submit
  eventHandler(keyCode) {
    if (keyCode === 13) {
      if (this.RegisterProject.valid) {
        // Trigger the button element with a click
        document.getElementById("send").click();
      }
    }
  }
  // Donde se guardan los datos del form
  saveData(data) {
    //valida que este lleno el formulario
    if (this.RegisterProject.valid) {
      this.CRUD.registerProject(data, this.imageCharge);
      this.RegisterProject.reset(); // borramos los datos del form
      this.imagePreview = [];
      this.logicReuse.presentToastSuccess('<ion-icon name="checkmark-circle"></ion-icon>  Su registro fue exitoso');
      this.router.navigate(['/home']);
      this.inputUntouched = false;
    } else {
      this.inputUntouched = true;
    }
  }
  // Condiciones de la entrada de datos form
  private FromRegisterProject() {
    return this.formBuilder.group({
      projectType: ['', Validators.required],
      projectName: ['', Validators.required],
      forestArea: ['', [Validators.required, Validators.pattern('[0-9].*'), Validators.min(1)]],
      forestType: ['', Validators.required],
      temperature: ['', [Validators.required, Validators.pattern('[0-9]*')]],
      typeTemperature: ['', Validators.required],
      treeDiameter: ['', Validators.required],
      treeAge: ['', Validators.required],
      treeSize: ['', Validators.required],
      country: ['', Validators.required],
      department: ['', Validators.required],
      municipality: ['', Validators.required],
      zone: ['', Validators.required],
      msnm: ['', [Validators.required]],
      propertyName: ['', Validators.required],
      cadastralCertificate: ['', Validators.required],
      enrollment: ['', Validators.required],
      treeSpecies: ['', Validators.required],
      uploadImage: ['', Validators.required],
    });
  }
  // Activa el evento ionChange para introducir datos a los selects
  oncountryChange() {
    let country = this.RegisterProject.get('country').value; //Pide el valor del Select pais
    this.Departmentchoose = this.logicReuse.CountryDepartment[country]; // introduce en el Select las opciones de acuerdo a lo escogido
    document.getElementById('department').style.display = "block";
    this._cdr.detectChanges();
  }
  departmentChange() {
    let department = this.RegisterProject.get('department').value;//Pide el valor del Select departamento
    this.Municipalitychoose = this.logicReuse.MunicipalDepartment[department];// introduce en el Select las opciones de acuerdo a lo escogido
    document.getElementById('municipality').style.display = "block";
    this._cdr.detectChanges();
  }
}
