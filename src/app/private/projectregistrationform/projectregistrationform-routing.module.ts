import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ProjectregistrationformPage } from './projectregistrationform.page';

const routes: Routes = [
  {
    path: '',
    component: ProjectregistrationformPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ProjectregistrationformPageRoutingModule {}
