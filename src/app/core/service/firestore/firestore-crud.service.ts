import { Injectable } from '@angular/core';
import { AuthService } from '../auth.service';
import { AngularFirestore, AngularFirestoreDocument } from "@angular/fire/firestore";
import { User } from 'src/app/shared/user.interface';
import { RegisterProject, IType } from 'src/app/shared/registrarproyecto.interface';
import { Sponsor } from 'src/app/shared/patrocinio.interface';
import { AngularFireStorage } from '@angular/fire/storage';
import { finalize } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { Pqr } from 'src/app/shared/pqr.interface';

@Injectable({
  providedIn: 'root'
})
export class FirestoreCRUDService {

  dataUser: any = [];
  detailsProduct: Array<RegisterProject> = [];
  date: Date = new Date();

  constructor(
    private authService: AuthService, private dataBase: AngularFirestore,
    private storage: AngularFireStorage, 
  ) {
    this.readDataUser();
  }
  // Obtener todos los proyectos registrados 
  getAllDocument(): Observable<firebase.firestore.QuerySnapshot> {
    return this.dataBase.collection<RegisterProject>('registerProject').get();
  }

  // Registar un proyecto en el banco de proyectos
  async registerProject(record, image) {
    // guarda un array de datos
    var projectBankImages: Array<string> = []
    image.forEach(element => {
      const randomId = Math.random().toString(36).substring(2, 8);
      var filePath = `project_bank_images/${randomId}`;
      var fileRef = this.storage.ref(filePath);// Creamos una referencia para uso posterior
      var task = this.storage.upload(filePath, element);//Aquí se guarda en el storage
      task.snapshotChanges()
        .pipe(// Cuando finalice la tarea de carga, pedimos la Url
          finalize(() => {
            fileRef.getDownloadURL().subscribe(urlImage => {
              projectBankImages.push(urlImage)
              var dataImage = {
                photos: projectBankImages
              }
              const userRef1 = this.dataBase.doc(`registerProject/${record.projectName}`);
              return userRef1.set(Object.assign({}, dataImage), { merge: true });
            });
          })
        ).subscribe(); // Nos suscribimos a los cambios para que se ejecute
    });

    const data: RegisterProject = {
      projectType: record.projectType,
      projectName: record.projectName,
      forestArea: record.forestArea,
      forestType: record.forestType,
      temperature: record.temperature,
      typeTemperature: record.typeTemperature,
      treeDiameter: record.treeDiameter,
      treeAge: record.treeAge,
      treeSize: record.treeSize,
      country: record.country,
      department: record.department,
      municipality: record.municipality,
      zone: record.zone,
      msnm: record.msnm,
      propertyName: record.propertyName,
      cadastralCertificate: record.cadastralCertificate,
      enrollment: record.enrollment,
      treeSpecies: record.treeSpecies,
      user: {
        email: this.authService.emailUser,
      },
      date: this.date,
      Validate: 'Sin aprobación'
    }
    const userRef: AngularFirestoreDocument<RegisterProject> = this.dataBase.doc(`registerProject/${data.projectName}`);
    return userRef.set(Object.assign({}, data), { merge: true });
  }
  //Actualizar imagen de perfil 
  async updateImageProfile(image) {
    try {
      let uidPhoto
      this.dataUser.forEach(element => {
        uidPhoto = element.photoProfile.uidPhoto
      });
      if (uidPhoto == undefined) {
        // Genera al azar un UID 
        const randomId = Math.random().toString(36).substring(2, 8);
        this.uploadImage(randomId, image)
      } else {
        this.uploadImage(uidPhoto, image)
      }
    } catch (error) {
      console.log('error ->', error)
    }
  }
  // Carga la url del storage y lo guarda en la base de datos
  async uploadImage(uidImage, image) {
    try {
      const filePath = `profile_picture/${uidImage}`;
      const fileRef = this.storage.ref(filePath);
      const task = this.storage.upload(filePath, image);
      task.snapshotChanges()
        .pipe(
          finalize(() => {
            fileRef.getDownloadURL().subscribe(urlImage => {
              const data: User = {
                photoProfile: {
                  photoURL: urlImage,
                  uidPhoto: uidImage
                }
              }
              const userRef: AngularFirestoreDocument<User> = this.dataBase.doc(`users/${this.authService.emailUser}`);
              return userRef.update(Object.assign({}, data));
            });
          })
        ).subscribe();
    } catch (error) {
      console.log('error ->', error)
    }
  }
  //Leer datos de usuario
  private readDataUser() {
    try {
      this.authService.user$.subscribe(docData => {
        this.dataUser.pop(); // volver aponer si hay problemas con el datauser
        this.dataUser.push(docData);
      });
    } catch (error) {
      console.log('error ->', error)
    }
  }
  // Actualizar datos del perfil
  async updateProfile(data) {
    try {
      const userRef: AngularFirestoreDocument<User> = this.dataBase.collection('users').doc(this.authService.emailUser);
      return userRef.update(data);
    } catch (error) {
      console.log('error ->', error)
    }
  }
  // registar un patrocinio y un aporte o donación
  async createSponsorship(record) {
    try {
      const userRef: AngularFirestoreDocument<Sponsor> = this.dataBase.doc(`sponsorship/${record.id}`);
      return userRef.set(Object.assign({}, record), { merge: true });
    } catch (error) {
      console.log('error ->', error)
    }
  }
  //Guardar PQRSD
  async savePQRSD(value) {
    try {
      const userRef: AngularFirestoreDocument<Pqr> = this.dataBase.doc(`PQR/${value.radicado}`);
      return userRef.set(Object.assign({}, value), { merge: true });
    } catch (error) {
      console.log('error ->', error)
    }
  }
  //borrar datos de la base de datos de usuarios 
  async deleteUser(email: string): Promise<void> {
    this.dataBase.doc('users' + '/' + email).delete().catch(err => console.log(err));
  }

}
