import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreDocument } from '@angular/fire/firestore';
import { Observable } from 'rxjs';
import { Sponsor } from 'src/app/shared/patrocinio.interface';
import { Pqr } from 'src/app/shared/pqr.interface';
import { RegisterProject } from 'src/app/shared/registrarproyecto.interface';
import { User } from 'src/app/shared/user.interface';

@Injectable({
  providedIn: 'root'
})
export class AdminDashboardService {

  detallesUser: User[] = [];
  detallesProject: RegisterProject[] = [];
  detallesSponsor: Sponsor[] = [];


  constructor(
    private dataBase: AngularFirestore,
  ) { }

  
  // Actualizar datos del perfil
  async updateProfile(data, email) {
    try {
      const userRef: AngularFirestoreDocument<User> = this.dataBase.doc(`users/${email}`);
      return userRef.update(data);
    } catch (error) {
      console.log('error ->', error)
    }
  }
    // Actualizar datos del Proyecto
  async updateProjects(data){
    try{
      const userRef: AngularFirestoreDocument<RegisterProject> = this.dataBase.doc(`registerProject/${data.projectName}`);
      return userRef.update(data);
    }  catch (error) {
      console.log('error ->', error)
    }
  }
  //Actualizar PQRSD
  async updatePQRSD(value) {
    try {
      const userRef: AngularFirestoreDocument<Pqr> = this.dataBase.doc(`PQR/${value.radicado}`);
      return userRef.update(Object.assign({}, value));
    } catch (error) {
      console.log('error ->', error)
    }
  }
  //Traer todos los documentos de la base de datos
  getAllSponsor(): Observable<firebase.firestore.QuerySnapshot> {
    try {
      return this.dataBase.collection<Sponsor>('sponsorship').get();
    } catch (error) {
      console.log('error ->', error)
    }
  }

  getAllUsers(): Observable<firebase.firestore.QuerySnapshot> {
    try {
      return this.dataBase.collection<User>('users').get();
    } catch (error) {
      console.log('error ->', error)
    }
  }

  getAllPQRSD(): Observable<firebase.firestore.QuerySnapshot> {
    try {
      return this.dataBase.collection<Pqr>('PQR').get();
    } catch (error) {
      console.log('error ->', error)
    }
  }

  async deletedocument(collection, record_id) {
    try {
      return this.dataBase.doc(collection + '/' + record_id).delete();
    } catch (error) {
      console.log('error ->', error)
    }
  }

}
