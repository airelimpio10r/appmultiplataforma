import { Injectable } from '@angular/core';

import { AngularFirestore, AngularFirestoreDocument } from "@angular/fire/firestore";
import { AlertController, ModalController, ToastController } from '@ionic/angular';
import { Observable, of } from 'rxjs';
import { switchMap } from 'rxjs/operators';
import { Authen, User } from '../../shared/user.interface';
import { auth } from 'firebase/app';
import { AngularFireAuth } from '@angular/fire/auth';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  public user$: Observable<User>;
  public emailUser: string = '';
  // public messageNoLogin: string;

  constructor(
    private _auth: AngularFireAuth, private toastController: ToastController,
    private dataBase: AngularFirestore,
  ) {
    this.stateLogin();
  }

  //Observa el estado de autentificación
  async stateLogin() {
    this.user$ = this._auth.authState.pipe(
      switchMap((user) => {
        if (user) {
          this.emailUser = user.email;
          //Mantiene el login
          this._auth.setPersistence(auth.Auth.Persistence.LOCAL);
          return this.dataBase.doc<User>(`users/${user.email}`).valueChanges();
        }
        this.emailUser = '';
        return of(null);
      })
    );
  }
  // Enviar Email de verificación
  async deleteUser() {
    try {
      return (await this._auth.currentUser).delete();
    } catch (error) {
      this.alertMessaje(error.message);
    }
  }
  //Reset de contraseña por medio de email
  async resetPassword(email): Promise<void> {  
    this._auth.sendPasswordResetEmail(email).catch(err => this.alertMessaje(err));
  }
  // Login with email and password
  async onLogin(value): Promise<Authen> {
    try {
      const { user } = await this._auth.signInWithEmailAndPassword(value.email, value.password);
      this.emailUser = user.email;
      const userRef: AngularFirestoreDocument<User> = this.dataBase.doc(`users/${user.email}`);
      userRef.set(Object.assign({}, value), { merge: true });
      return user;
    } catch (error) {
      console.log('Error->', error);
      this.alertMessaje(error.message);
    }
  }
  //Register with email and password
  async onRegister(data): Promise<Authen> {
    try {
      const { user } = await this._auth.createUserWithEmailAndPassword(data.email, data.password);
      const userRef: AngularFirestoreDocument<User> = this.dataBase.doc(`users/${user.email}`);
      userRef.set(Object.assign({}, data), { merge: true });
      this.sendEmailVerification();
      this.emailUser = user.email;
      return user;
    } catch (error) {
      this.alertMessaje(error.message);
    }
  }
  // Enviar Email de verificación
  async sendEmailVerification() {
    return (await this._auth.currentUser).sendEmailVerification();
  }
  //cerrar sesión 
  async logout(): Promise<void> {
    try {
      this.emailUser = '';
      return await this._auth.signOut();
    } catch (error) {
      this.alertMessaje(error.message);
    }
  }

  //POP-UP
  //Mensaje de Validación de warning
  async alertMessaje(message: string) {
    const toast = await this.toastController.create({
      // message: "El usuario no se encuentra registrado", // No se puede aqui estoy manejando los errores de Firebase 
      message: message,
      duration: 5000,
      position: 'middle',
      color: 'warning',
      animated: true,
      cssClass: "my-custom-class"
    });
    toast.present();
  }
}