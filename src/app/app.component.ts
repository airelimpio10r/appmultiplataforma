
import { LoadingController, MenuController, NavController, Platform } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { AuthService } from '../app/core/service/auth.service';
import { Component, OnInit } from '@angular/core';
import { FirestoreCRUDService } from './core/service/firestore/firestore-crud.service';
import { Router } from '@angular/router';

import { Storage } from '@ionic/storage';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss']
})
export class AppComponent implements OnInit {
  
  activateDashboar: boolean = false;
  Photo: string;
  Name: string;
  pages: any;
  public selectedIndex = 0;
  public appPages = [
    {
      title: 'Inicio',
      url: 'home',
      icon: 'Home'
    },
    {
      title: 'Iniciativas 10R',
      url: 'intro',
      icon: 'apps'
    },
    {
      title: 'Nosotros',
      url: 'aboutus',
      icon: 'heart'
    },
    {
      title: 'Registro de proyecto',
      url: 'projectregistrationform',
      icon: 'add-circle'
    },
    {
      title: 'Banco de proyectos',
      url: 'allprojects',
      icon: 'layers'
    },
    {
      title: 'Patrocinadores',
      url: 'patrocinadores',
      icon: 'ribbon'
    },
    {
      title: 'Huella de carbono',
      url: 'carbonfootprintform',
      icon: 'leaf'
    },
    {
      title: 'Beneficios',
      url: 'benefits',
      icon: 'pricetag'
    },
    {
      title: 'Iniciar sesión',
      url: 'login',
      icon: 'log-in'
    },
  ];

  constructor(
    private platform: Platform, private splashScreen: SplashScreen,
    private statusBar: StatusBar, private authService: AuthService,
    private CRUD: FirestoreCRUDService, private router: Router,
    private menu: MenuController, private storage: Storage,
    public loadingController: LoadingController
  ) {
    this.initializeApp();
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }
  async presentLoading() {
    const loading = await this.loadingController.create({
      cssClass: 'my-custom-class',
      message: 'Cargando',
      //duration: 2000
    });
    await loading.present();
    const { role, data } = await loading.onDidDismiss();
  }
  ngOnInit() {
    const path = window.location.pathname.split('home')[1];
    if (path !== undefined) {
      this.selectedIndex = this.appPages.findIndex(page => page.title.toLowerCase() === path.toLowerCase());
    }
    this.getCurrentUser();
  }
  //Dar función a cerrar sesión 
  onLogout(selectedIndex) {
    if (selectedIndex == 'Cerrar sesión') {
      this.authService.logout();
    }
  }
  // Obtener datos del usuario
  getCurrentUser() {
    this.Photo = '/assets/images/defaul-Image.png';
    this.authService.user$.subscribe(data => {
      if (data) {
        this.Photo = data.photoProfile.photoURL;
        this.Name = data.name;
        if(data.rol == 'Admin'){
          this.activateDashboar = true;
        }
        this.appPages.pop();
        this.appPages.push(
          {
            title: 'Cerrar sesión',
            url: '/home',
            icon: 'log-out',
          }
        )
      } else {
        this.activateDashboar = false;
        this.Photo = '/assets/images/defaul-Image.png';
        this.Name = 'Bienvenido';
        this.CRUD.dataUser.pop();
        this.appPages.pop();
        this.appPages.push(
          {
            title: 'Iniciar sesión',
            url: 'login',
            icon: 'log-in'
          }
        )
      }
    });
  }
  // Activar el tutorial
  openTutorial() {
    this.menu.enable(false);
    this.storage.set('ion_did_tutorial', false);
    this.router.navigateByUrl('/tutorial');
  }
}